#include "chesspiecetest.h"
#include <color.h>
#include <component.h>
#include <namer.h>
#include <piece.h>
#include <types/chess/rook.h>
#include <types/chess/bishop.h>
#include <types/chess/queen.h>
#include <types/chess/king.h>
#include <variant.h>

using namespace Chess;

CPPUNIT_TEST_SUITE_REGISTRATION(ChessPieceTest);

Variant* chess_variant_factory();

void ChessPieceTest::setUp() {
  m_chess = chess_variant_factory();
  m_namer = requestComponent<INamer>(m_chess, "namer");
}

void ChessPieceTest::tearDown() {
  delete m_chess;
}

void ChessPieceTest::test_basic() {
  Piece p(White::self(), Rook::self());
  CPPUNIT_ASSERT_EQUAL(
    static_cast<const IColor*>(White::self()),
    p.color());
  CPPUNIT_ASSERT_EQUAL(
    static_cast<const IType*>(Rook::self()),
    p.type());
}

void ChessPieceTest::test_names() {
  Piece p(Black::self(), Bishop::self());
  CPPUNIT_ASSERT(p.color()->name() == "black");
  CPPUNIT_ASSERT(p.type()->name() == "bishop");
  CPPUNIT_ASSERT(m_namer->name(p) == "black_bishop");
}

void ChessPieceTest::test_compare() {
  Piece a(Black::self(), King::self());
  Piece b(White::self(), Queen::self());
  Piece c(Black::self(), King::self());
  
  CPPUNIT_ASSERT(a != b);
  CPPUNIT_ASSERT(!(a == b));
  CPPUNIT_ASSERT(a == c);
  CPPUNIT_ASSERT(c == a);
  CPPUNIT_ASSERT(a == a);
}


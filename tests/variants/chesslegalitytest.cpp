#include "chesslegalitytest.h"

#include <board.h>
#include <color.h>
#include <defaultstate.h>
#include <move.h>
#include <piece.h>
#include <statefactory.h>
#include <types/chess/pawn.h>
#include <types/chess/rook.h>
#include <types/chess/king.h>
#include <types/chess/queen.h>
#include <validator.h>
#include <variant.h>

#include "test_utils.h"

using namespace Chess;

Variant* chess_variant_factory();

CPPUNIT_TEST_SUITE_REGISTRATION(ChessLegalityTest);

void ChessLegalityTest::setUp() {
  m_chess = chess_variant_factory();
  IStateFactory* fact = requestComponent<IStateFactory>(
    m_chess, "state_factory");
  m_state = dynamic_cast<IDefaultState*>(fact->createState());
  m_validator = requestComponent<IValidator>(
    m_chess, "validator");

  m_state->setup();
}

void ChessLegalityTest::tearDown() {
  delete m_state;
  delete m_chess;
}

void ChessLegalityTest::test_movements() {
  Move e4(Point(4, 6), Point(4, 4));
  CPPUNIT_ASSERT(m_validator->legal(m_state, e4));
  CPPUNIT_ASSERT(e4.type() == "en_passant_trigger");
    
  Move crazy(Point(0, 0), Point(7, 4));
  CPPUNIT_ASSERT(!m_validator->pseudolegal(m_state, crazy));
}

void ChessLegalityTest::test_pseudolegal() {
  Move e4(Point(4, 6), Point(4, 4));
  CPPUNIT_ASSERT(m_validator->pseudolegal(m_state, e4));
  
  Move e5(Point(4, 6), Point(4, 3));
  CPPUNIT_ASSERT(!m_validator->pseudolegal(m_state, e5));
  
  Move Nf3(Point(6, 7), Point(5, 5));
  CPPUNIT_ASSERT(m_validator->pseudolegal(m_state, Nf3));
  
  Move Bc4(Point(5, 7), Point(2, 4));
  CPPUNIT_ASSERT(!m_validator->pseudolegal(m_state, Bc4));
  
  // black moves
  m_state->advanceTurn();
  
  Move crazy(Point(0, 0), Point(4, 7));
  CPPUNIT_ASSERT(!m_validator->pseudolegal(m_state, crazy));
  
  Move e5_(Point(4, 1), Point(4, 3));
  CPPUNIT_ASSERT(m_validator->pseudolegal(m_state, e5_));
}

void ChessLegalityTest::test_simple_move() {
  Move e4(Point(4, 6), Point(4, 4));
  CPPUNIT_ASSERT(m_validator->legal(m_state, e4));
  
  Move e5(Point(4, 6), Point(4, 3));
  CPPUNIT_ASSERT(!m_validator->pseudolegal(m_state, e5));
  
  Move Nf3(Point(6, 7), Point(5, 5));
  CPPUNIT_ASSERT(m_validator->legal(m_state, Nf3));
  
  Move Bc4(Point(5, 7), Point(2, 4));
  CPPUNIT_ASSERT(!m_validator->pseudolegal(m_state, Bc4));
  
  {
    Move tmp(Point(5, 7), Point(2, 4));
    m_state->board()->set(Point(4, 6), Piece());
    CPPUNIT_ASSERT(m_validator->legal(m_state, tmp));
  }
  
  Move e6(Point(4, 1), Point(4, 2));
  CPPUNIT_ASSERT(!m_validator->pseudolegal(m_state, e6));
  
  {
    Move tmp(Point(4, 1), Point(4, 2));
    m_state->advanceTurn();
    CPPUNIT_ASSERT(m_validator->legal(m_state, tmp));
  }
}

void ChessLegalityTest::test_promotion() {
  m_state->board()->set(Point(7, 1), Piece(White::self(), Pawn::self()));
  Move h8Q(Point(7, 1), Point(7, 0), Queen::self());
  CPPUNIT_ASSERT(!m_validator->pseudolegal(m_state, h8Q));
  
  Move hxg8R(Point(7, 1), Point(6, 0), Rook::self());
  CPPUNIT_ASSERT(m_validator->pseudolegal(m_state, hxg8R));
  CPPUNIT_ASSERT(m_validator->legal(m_state, hxg8R));
  CPPUNIT_ASSERT_EQUAL(
    static_cast<const IType*>(Rook::self()),
    hxg8R.promotion());
}

void ChessLegalityTest::test_en_passant() {
  m_state->move(Move(Point(4, 6), Point(4, 4)));
  m_state->move(Move(Point(7, 1), Point(7, 2)));
  m_state->move(Move(Point(4, 4), Point(4, 3)));
  
  Move d5(Point(3, 1), Point(3, 3));
  CPPUNIT_ASSERT(m_validator->legal(m_state, d5));
  CPPUNIT_ASSERT(d5.type() == "en_passant_trigger");
  m_state->move(d5);
  
  Move exd6(Point(4, 3), Point(3, 2));
  CPPUNIT_ASSERT(m_validator->legal(m_state, exd6));
  CPPUNIT_ASSERT(m_state->captureSquare(exd6) == Point(3, 3));
  
  m_state->move(Move(Point(7, 6), Point(7, 5)));
  m_state->move(Move(Point(7, 2), Point(7, 3)));
  
  Move tmp(Point(4, 3), Point(3, 2));
  CPPUNIT_ASSERT(!m_validator->pseudolegal(m_state, tmp));
}

void ChessLegalityTest::test_castling() {
  Move oo(Point(4, 7), Point(6, 7));
  
  m_state->board()->set(Point(6, 7), Piece());
  {
    Move tmp(oo);
    CPPUNIT_ASSERT(!m_validator->pseudolegal(m_state, tmp));
  }
  
  m_state->board()->set(Point(5, 7), Piece());
  {
    Move tmp(oo);
    CPPUNIT_ASSERT(m_validator->legal(m_state, tmp));
  }
  
  m_state->board()->set(Point(3, 6), Piece(Black::self(), Pawn::self()));
  {
    Move tmp(oo);
    CPPUNIT_ASSERT(!m_validator->pseudolegal(m_state, tmp));
  }
  
  m_state->board()->set(Point(3, 6), Piece());
  m_state->board()->set(Point(4, 6), Piece(Black::self(), Pawn::self()));
  {
    Move tmp(oo);
    CPPUNIT_ASSERT(!m_validator->pseudolegal(m_state, tmp));
  }
}

void ChessLegalityTest::test_attack1() {
  m_state->board()->set(Point(5, 5), Piece(Black::self(), Rook::self()));
  CPPUNIT_ASSERT(m_validator->attacks(m_state, White::self(), Point(5, 5)));  
}

void ChessLegalityTest::test_attack2() {
  m_state->board()->set(Point(4, 4), Piece(Black::self(), Queen::self()));
  CPPUNIT_ASSERT(!m_validator->attacks(m_state, White::self(), Point(4, 4)));
}

void ChessLegalityTest::test_attack3() {
  m_state->board()->set(Point(0, 5), Piece(Black::self(), King::self()));
  m_state->board()->set(Point(1, 6), Piece(White::self(), Rook::self())); // b2: pawn -> rook
  CPPUNIT_ASSERT(m_validator->attacks(m_state, White::self(), Point(0, 5)));
  
  m_state->board()->set(Point(1, 7), Piece()); // remove knight on b1
  CPPUNIT_ASSERT(!m_validator->attacks(m_state, White::self(), Point(0, 5)));
  
  m_state->board()->set(Point(0, 6), Piece()); // remove pawn on a2
  CPPUNIT_ASSERT(m_validator->attacks(m_state, White::self(), Point(0, 5)));
  
  m_state->board()->set(Point(0, 7), Piece()); // remove rook on a1
  CPPUNIT_ASSERT(!m_validator->attacks(m_state, White::self(), Point(0, 5)));
}

void ChessLegalityTest::test_attack4() {
  CPPUNIT_ASSERT(!m_validator->attacks(m_state, Black::self(), Point(4, 7)));
}


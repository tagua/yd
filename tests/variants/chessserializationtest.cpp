#include "chessserializationtest.h"
#include <board.h>
#include <color.h>
#include <defaultstate.h>
#include <move.h>
#include <moveserializer.h>
#include <piece.h>
#include <statefactory.h>
#include <types/chess/bishop.h>
#include <types/chess/king.h>
#include <types/chess/pawn.h>
#include <types/chess/queen.h>
#include <types/chess/rook.h>
#include <validator.h>
#include <variant.h>

#include "test_utils.h"

using namespace Chess;

CPPUNIT_TEST_SUITE_REGISTRATION(ChessSerializationTest);

Variant* chess_variant_factory();

void ChessSerializationTest::setUp() {
  m_chess = chess_variant_factory();
  IStateFactory* fact = requestComponent<IStateFactory>(
    m_chess, "state_factory");
  m_state = dynamic_cast<IDefaultState*>(fact->createState());
  m_validator = requestComponent<IValidator>(
    m_chess, "validator");
  m_decorator = requestComponent<IMoveSerializer>(
    m_chess, "move_serializer/decorated");
  m_san = requestComponent<IMoveSerializer>(
    m_chess, "move_serializer/san");
  m_simple = requestComponent<IMoveSerializer>(
    m_chess, "move_serializer/simple");
}

void ChessSerializationTest::tearDown() {
  delete m_state;
  delete m_chess;
}

void ChessSerializationTest::test_pawn() {
  m_state->setup();
  
  Move move(Point(4, 6), Point(4, 4));
  CPPUNIT_ASSERT(m_validator->legal(m_state, move));
  
  CPPUNIT_ASSERT_EQUAL(QString("e4"), m_san->serialize(move, m_state));
    
  CPPUNIT_ASSERT_EQUAL(QString("e2e4"), m_simple->serialize(move, m_state));
}

void ChessSerializationTest::test_check() {
  m_state->board()->set(Point(5, 5), Piece(Black::self(), King::self()));
  m_state->board()->set(Point(0, 0), Piece(White::self(), King::self()));
  m_state->board()->set(Point(3, 5), Piece(White::self(), Bishop::self()));
  
  Move move(Point(3, 5), Point(4, 4));
  CPPUNIT_ASSERT(m_validator->legal(m_state, move));
  
  CPPUNIT_ASSERT_EQUAL(QString("Be4+"), m_san->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("d3e4"), m_simple->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("{bishop}e4+"), m_decorator->serialize(move, m_state));
}

void ChessSerializationTest::test_check_capture() {
  m_state->board()->set(Point(5, 5), Piece(Black::self(), King::self()));
  m_state->board()->set(Point(0, 0), Piece(White::self(), King::self()));
  m_state->board()->set(Point(3, 5), Piece(White::self(), Bishop::self()));
  m_state->board()->set(Point(4, 4), Piece(Black::self(), Rook::self()));
  
  Move move(Point(3, 5), Point(4, 4));
  CPPUNIT_ASSERT(m_validator->legal(m_state, move));
  
  CPPUNIT_ASSERT_EQUAL(QString("Bxe4+"), m_san->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("d3e4"), m_simple->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("{bishop}xe4+"), m_decorator->serialize(move, m_state));
}

void ChessSerializationTest::test_promotion() {
  m_state->board()->set(Point(4, 7), Piece(Black::self(), King::self()));
  m_state->board()->set(Point(0, 0), Piece(White::self(), King::self()));
  m_state->board()->set(Point(7, 1), Piece(White::self(), Pawn::self()));
  
  Move move(Point(7, 1), Point(7, 0), Rook::self());
  CPPUNIT_ASSERT(m_validator->legal(m_state, move));
  
  CPPUNIT_ASSERT_EQUAL(QString("h8=R"), m_san->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("h7h8=R"), m_simple->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("h8={rook}"), m_decorator->serialize(move, m_state));
}

void ChessSerializationTest::test_promotion_capture() {
  m_state->board()->set(Point(4, 7), Piece(Black::self(), King::self()));
  m_state->board()->set(Point(0, 0), Piece(White::self(), King::self()));
  m_state->board()->set(Point(7, 1), Piece(White::self(), Pawn::self()));
  m_state->board()->set(Point(6, 0), Piece(Black::self(), Bishop::self()));
  
  Move move(Point(7, 1), Point(6, 0), Rook::self());
  CPPUNIT_ASSERT(m_validator->legal(m_state, move));
  
  CPPUNIT_ASSERT_EQUAL(QString("hxg8=R"), m_san->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("h7g8=R"), m_simple->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("hxg8={rook}"), m_decorator->serialize(move, m_state));
}

void ChessSerializationTest::test_promotion_check() {
  m_state->board()->set(Point(4, 0), Piece(Black::self(), King::self()));
  m_state->board()->set(Point(0, 0), Piece(White::self(), King::self()));
  m_state->board()->set(Point(7, 1), Piece(White::self(), Pawn::self()));
  
  Move move(Point(7, 1), Point(7, 0), Rook::self());
  CPPUNIT_ASSERT(m_validator->legal(m_state, move));
  
  CPPUNIT_ASSERT_EQUAL(QString("h8=R+"), m_san->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("h7h8=R"), m_simple->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("h8={rook}+"), m_decorator->serialize(move, m_state));
}

void ChessSerializationTest::test_promotion_capture_check() {
  m_state->board()->set(Point(4, 0), Piece(Black::self(), King::self()));
  m_state->board()->set(Point(0, 0), Piece(White::self(), King::self()));
  m_state->board()->set(Point(7, 1), Piece(White::self(), Pawn::self()));
  m_state->board()->set(Point(6, 0), Piece(Black::self(), Bishop::self()));
  
  Move move(Point(7, 1), Point(6, 0), Rook::self());
  CPPUNIT_ASSERT(m_validator->legal(m_state, move));
  
  CPPUNIT_ASSERT_EQUAL(QString("hxg8=R+"), m_san->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("h7g8=R"), m_simple->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("hxg8={rook}+"), m_decorator->serialize(move, m_state));
}

void ChessSerializationTest::test_castling_k() {
  m_state->setup();
  
  m_state->board()->set(Point(5, 7), Piece());
  m_state->board()->set(Point(6, 7), Piece());
  
  Move move(Point(4, 7), Point(6, 7));
  CPPUNIT_ASSERT(m_validator->legal(m_state, move));
  
  CPPUNIT_ASSERT_EQUAL(QString("O-O"), m_san->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("e1g1"), m_simple->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("O-O"), m_decorator->serialize(move, m_state));
}

void ChessSerializationTest::test_castling_q() {
  m_state->setup();
  
  m_state->board()->set(Point(3, 7), Piece());
  m_state->board()->set(Point(2, 7), Piece());
  m_state->board()->set(Point(1, 7), Piece());
  
  Move move(Point(4, 7), Point(2, 7));
  CPPUNIT_ASSERT(m_validator->legal(m_state, move));
  
  CPPUNIT_ASSERT_EQUAL(QString("O-O-O"), m_san->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("e1c1"), m_simple->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("O-O-O"), m_decorator->serialize(move, m_state));
}

void ChessSerializationTest::regression_knight_king() {
  m_state->setup();
  
  Move move(Point(6, 7), Point(5, 5));
  CPPUNIT_ASSERT(m_validator->legal(m_state, move));

  CPPUNIT_ASSERT_EQUAL(QString("Nf3"), m_san->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("g1f3"), m_simple->serialize(move, m_state));
  
  CPPUNIT_ASSERT_EQUAL(QString("{knight}f3"), m_decorator->serialize(move, m_state));
}


#include "behaviour.h"
#include "board.h"
#include "defaultstate.h"
#include "defaulttype.h"

std::vector<const Point*> DefaultState::theoreticalMoves(const Piece& piece,
							 const Point& src) const {
  std::vector<const Point*> destinations;

  const IBehaviour* behaviour = this->behaviour();
  if (!behaviour) return destinations; // empty

  const std::vector<MoveDefinition> * defs =
    dynamic_cast<const DefaultType*>(piece.type())->movesDefinitions();
  if (!defs) return destinations; // empty

  //kDebug() << "looking at moves from " << src;
  for (unsigned direction=0; direction < defs->size(); direction++)
    for (int distance=1;
	 (*defs)[direction].distance ? (distance <= (*defs)[direction].distance) : 1;
	 distance++) {
      Point* candidate = new Point(src.x + ((*defs)[direction].x * distance *
					    -behaviour->direction(piece.color()).y),
				   src.y + ((*defs)[direction].y * distance *
					    behaviour->direction(piece.color()).y));
      if (!board()->valid(*candidate)) {
	// end of board: don't look further in that direction
	delete candidate;
	break;
      }

      destinations.push_back(candidate);

      // stop looking in direction if there is a piece blocking move here
      const Piece otherPiece = board()->get(*candidate);
      if (otherPiece != Piece())
	break;
  }

  return destinations;
}

/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "piece.h"

Piece::Piece(const IColor* color, const IType* type)
: m_type(type)
, m_color(color) { }

const IColor* Piece::color() const { return m_color; }

const IType* Piece::type() const { return m_type; }
void Piece::setType(const IType* type) { m_type = type; }

bool Piece::operator==(const Piece& piece) const {
  return m_type == piece.type() &&
         m_color == piece.color();
}

bool Piece::operator!=(const Piece& piece) const {
  return !(operator==(piece));
}

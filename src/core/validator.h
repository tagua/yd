/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__VALIDATOR_H
#define CORE__VALIDATOR_H

#include "piece.h"
#include "point.h"

class IState;
class Move;

/**
  * @brief A component to check legality of moves.
  */
class TAGUA_EXPORT IValidator {
public:
  virtual ~IValidator();
  
  /**
    * A move is pseudolegal if it obeys all rules
    * except those regarding king safety.
    * @returns Whether the given move is pseudolegal.
    */
  virtual bool pseudolegal(const IState* state, Move& move) const = 0;
  
  /**
    * A move if legal if it obeys all the rules of the game.
    * @returns Whether the given move is legal.
    */
  virtual bool legal(const IState* state, Move& move) const = 0;
  
  /**
    * Check if some piece of @a player attacks the piece @a target
    * on the given square.
    * If the @a target parameter is not given, whatever piece is in 
    * the specified square is considered the target.
    * @param state The reference state of the game.
    * @param player The attacking player.
    * @param square The destination square of the attack.
    * @param target An optional piece to be considered as the target for the attack.
    */
  virtual bool attacks(const IState* state, const IColor* player, 
                       const Point& square, const Piece& target = Piece()) const = 0;
    
  /**
    * @return The owner of the moving piece.
    */
  virtual const IColor* mover(const IState* state, const Move& move) const = 0;
  
  virtual void setDelegator(IValidator* delegator) = 0;
};

#endif // CORE__VALIDATOR_H

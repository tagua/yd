/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__PIECE_H
#define CORE__PIECE_H

#include <QMetaType>
#include "taguaobject.h"

class IType;
class IColor;

class TAGUA_EXPORT Piece : public TaguaObject {
  const IType* m_type;
  const IColor* m_color;
public:
  Piece(const IColor* color = 0, const IType* type = 0);
  
  const IColor* color() const;
  const IType* type() const;
  void setType(const IType* type);
  
  bool operator==(const Piece& other) const;
  bool operator!=(const Piece& other) const;
};

Q_DECLARE_METATYPE(Piece)

#endif // CORE__PIECE_H


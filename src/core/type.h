/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__TYPE_H
#define CORE__TYPE_H

#include <QString>
#include "export.h"

class Piece;
class Move;
class IState;

/**
  * @brief A piece type.
  * 
  * Piece types are associated to graphical representation of pieces,
  * and to movement rules.
  */
class TAGUA_EXPORT IType {
public:
  virtual ~IType();
  
  /**
    * @return The name of this type.
    */
  virtual QString name() const = 0;
  
  /**
    * Check if this piece type can perform a given move.
    * @param piece A piece of this type.
    * @param target The piece that would be captured by the move.
    * @param move The move to be checked.
    * @param state The state in which @a move is to be played.
    */
  virtual bool canMove(const Piece& piece, const Piece& target, 
                       Move& move, const IState* state) const = 0;
  
  /**
    * A symbolic index associated to this type.
    * This is not required to uniquely identify the type.
    * It can be used to sort a collection of types.
    */
  virtual int index() const = 0;
};

#endif // CORE__TYPE_H


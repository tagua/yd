#include "promotionmanager.h"

void PromotionManager::setPromotion(IType* from, IType* to)
{
  m_promotions[from] = to;
  m_depromotions[to] = from;
}

const IType * PromotionManager::getPromotion(const IType* p) const
{
  return m_promotions[p];
}

const IType * PromotionManager::getDepromotion(const IType* p) const
{
  return m_depromotions[p];
}

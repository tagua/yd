/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__DEFAULTPOOLCOLLECTION_H
#define CORE__DEFAULTPOOLCOLLECTION_H

#include <QMap>

#include "component.h"
#include "poolcollection.h"

class TAGUA_EXPORT DefaultPoolCollection
: public Component
, public IPoolCollection {
  typedef QMap<const IColor*, IPool*> PoolMap;
  PoolMap m_pools;  
public:
  DefaultPoolCollection();
  virtual ~DefaultPoolCollection();
  
  virtual IPoolCollection* clone() const;
  virtual bool equals(const IPoolCollection* other) const;
  virtual IPool* pool(const IColor* player);
  virtual const IPool* pool(const IColor* player) const;
  
  virtual void addPool(const IColor* color, IPool* pool);
};

#endif // CORE__DEFAULTPOOLCOLLECTION_H


/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "animation.h"

Animation::~Animation() { }

bool Animation::sticky() const { return false; }

void Animation::addPreAnimation(AnimationPtr) { }

void Animation::addPostAnimation(AnimationPtr) { }


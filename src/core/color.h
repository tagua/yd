/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__COLOR_H
#define CORE__COLOR_H

#include "component.h"

/**
  * @brief A player in a game.
  * 
  * Each color represents a different player, and possibly the ownership
  * of pieces on the board.
  */
class TAGUA_EXPORT IColor {
public:
  virtual ~IColor();
  
  /**
    * @return The color name.
    */
  virtual QString name() const = 0;
  
  /**
    * @return The color index.
    */
  virtual int index() const = 0;
};

class TAGUA_EXPORT White : public Component, public IColor {
Q_OBJECT
  White();
public:
  virtual QString name() const;
  virtual int index() const;
  
  static White* self();
};

class TAGUA_EXPORT Black : public Component, public IColor {
Q_OBJECT
  Black();
public:
  virtual QString name() const;
  virtual int index() const;
  
  static Black* self();
};

#endif // CORE__COLOR_H

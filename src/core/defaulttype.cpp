#include "defaultstate.h"
#include "defaulttype.h"
#include "move.h"

#include <KDebug>

bool DefaultType::canMove(const Piece& piece, const Piece&,
			  Move& move, const IState* state) const {
  const DefaultState* dstate = dynamic_cast<const DefaultState*>(state);
  if (dstate == NULL) {
    kDebug() << "use of a non-DefaultState-derived state with DefaultType";
    return false;
  }

  std::vector<const Point*> moves = dstate->theoreticalMoves(piece, move.src());

  // theoreticalMoves() allocates Point objects for us, we must ensure
  // all of them are freed, so we visit the whole moves vector.
  bool ret = false;
  for (std::vector<const Point*>::const_iterator it = moves.begin();
       it != moves.end(); it++) {
    if (move.dst() == **it)
      ret = true;
    delete *it;
  }

  return ret;
}

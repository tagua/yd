/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__STATE_FACTORY_H
#define CORE__STATE_FACTORY_H

#include "export.h"

class IState;

class TAGUA_EXPORT IStateFactory {
public:
  virtual ~IStateFactory();
  
  /**
    * Create an empty state for this game. An empty state
    * is usually a board with no pieces, empty pools and
    * flags in their default values.
    * An empty state is generally not the initial state for
    * a game. 
    * 
    * To get the initial state, call reset on an
    * empty state.
    * 
    * @return The newly created state.
    * @sa IState::reset
    */
  virtual IState* createState() const = 0;
};

#endif // CORE__STATE_FACTORY_H


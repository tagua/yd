/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__POOL_COLLECTION_H
#define CORE__POOL_COLLECTION_H

class IColor;
class IPool;

/**
  * @brief An association player -> pool.
  */
class IPoolCollection {
public:
  virtual ~IPoolCollection();
  
  virtual IPoolCollection* clone() const = 0;
  
  /**
    * @return Whether two pool collections are equal.
    */
  virtual bool equals(const IPoolCollection* other) const = 0;
  
  /**
    * @return The pool of the given player.
    */
  virtual IPool* pool(const IColor* player) = 0;
  virtual const IPool* pool(const IColor* player) const = 0;
};

#endif // CORE__POOL_COLLECTION_H


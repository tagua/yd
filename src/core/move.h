/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__MOVE_H
#define CORE__MOVE_H

#include <QMetaType>
#include "taguaobject.h"
#include "point.h"
#include "piece.h"

class TAGUA_EXPORT Move : public TaguaObject {
  Point m_src;
  Point m_dst;
  const IType* m_promotion;
  
  // for drops
  const IColor* m_pool;
  int m_index;
  Piece m_drop;
  
  QString m_type;
public:
  Move();
  Move(const Point& src, const Point& dst, const IType* promotion = 0);
  Move(const IColor* pool, int index, const Point& dst);
    
  Point src() const;
  Point dst() const;
  Piece drop() const;
  void setDrop(const Piece& piece);
  Point delta() const;
  
  QString type() const;
  void setType(const QString&);
  
  const IType* promotion() const;
  void setPromotion(const IType* promotion);
  
  bool isDrop() const;
  
  const IColor* pool() const;
  int index() const;
  
  bool operator==(const Move& mv) const;
  bool operator!=(const Move& mv) const;
};

Q_DECLARE_METATYPE(Move)

#endif // CORE__MOVE_H

/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__POLICY_H
#define CORE__POLICY_H

#include "interactiontype.h"
#include "turnpolicy.h"

class IState;
class IType;
class Point;

/**
  * @brief A policy defines what pieces are movable in a given TurnTest.
  */
class TAGUA_EXPORT IPolicy {
public:
  virtual ~IPolicy();
    
  /**
    * @return Whether the piece in the given square can be moved.
    */
  virtual InteractionType movable(const IState* state,
                                  const TurnTest& test, 
                                  const Point& p) const = 0;

  /**
    * @return Whether it is possible to drop from the given pool.
    */
  virtual InteractionType droppable(const IState* state,
                                  const TurnTest& test, 
                                  const IColor* pool) const = 0;
};

#endif // CORE__POLICY_H


/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CORE__DEFAULTPOOL_H
#define CORE__DEFAULTPOOL_H

#include <QMap>

#include "pool.h"
#include "export.h"

class IColor;
class IType;

/**
  * @brief Convenience IPool implementation for shogi-like games.
  * 
  * A DefaultPool holds pieces of the same color. It doesn't
  * actually contains pieces, but creates them on the fly whenever it
  * is asked one.
  * 
  * Pieces are maintained using an association type -> number of pieces
  * of that type. Indexes are computed as if all pieces where ordered by
  * type.
  */
class TAGUA_EXPORT DefaultPool : public IPool {
  /**
    * @brief Wrapper of an IType pointer, used to sort types by index.
    */
  struct Key {
  public:
    const IType* type;
    Key(const IType* type);
    bool operator<(const Key& other) const;
  };
  typedef QMap<Key, int> Data;
  Data m_data;
  const IColor* m_owner;
public:
  DefaultPool(const IColor* owner);
  virtual ~DefaultPool();
    
  virtual IPool* clone() const;
  virtual bool equals(const IPool* other) const;
  virtual bool empty() const;
  virtual int size() const;
  virtual int insert(int index, const Piece& piece);
  virtual Piece get(int index) const;
  virtual Piece take(int index);
  virtual bool take(const Piece& piece);
  
  /**
    * @return The common color of the pieces.
    */
  virtual const IColor* color() const;
  
  /**
    * @return The number of pieces of the given type.
    */
  virtual int count(const IType* type) const;
  
  /**
    * Add a piece of the given type to the pool.
    * @return The new number of pieces of the given type.
    */
  virtual int add(const IType* type);
  
  /**
    * Remove a piece of the given type from the pool.
    * @return The new number of pieces of the given type.
    */
  virtual int remove(const IType* type);
};

#endif // CORE__DEFAULTPOOL_H

/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>
            
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "move.h"

Move::Move()
: m_src(Point::invalid())
, m_dst(Point::invalid())
, m_promotion(0)
, m_pool(0)
, m_index(-1) { }

Move::Move(const Point& src, const Point& dst, const IType* promotion)
: m_src(src)
, m_dst(dst)
, m_promotion(promotion)
, m_pool(0)
, m_index(-1) { }

Move::Move(const IColor* pool, int index, const Point& dst)
: m_src(Point::invalid())
, m_dst(dst)
, m_promotion(0)
, m_pool(pool)
, m_index(index) { }

Point Move::src() const { return m_src; }

Point Move::dst() const { return m_dst; }

Point Move::delta() const { return dst() - src(); }

Piece Move::drop() const { return m_drop; }

void Move::setDrop(const Piece& piece) { m_drop = piece; }

bool Move::isDrop() const { return m_pool != 0 && m_index != -1; }
  
const IColor* Move::pool() const { return m_pool; }

int Move::index() const { return m_index; }

QString Move::type() const { return m_type; }

void Move::setType(const QString& type) { m_type = type; }

const IType* Move::promotion() const { return m_promotion; }

void Move::setPromotion(const IType* prom) { m_promotion = prom; }

bool Move::operator==(const Move& mv) const {
  if (!TaguaObject::operator==(mv)) return false;
  if (m_type != mv.m_type) return false;
        
  if (m_pool) {
    return m_pool == mv.m_pool &&
           m_index == mv.m_index &&
           m_dst == mv.m_dst &&
           m_promotion == mv.m_promotion;
  }
  else {
    return m_src == mv.m_src &&
           m_dst == mv.m_dst &&
           m_promotion == mv.m_promotion;
  }
}

bool Move::operator!=(const Move& mv) const {
  return !(operator==(mv));
}

/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef DELEGATORS__MOVESERIALIZER_H
#define DELEGATORS__MOVESERIALIZER_H

#include "../moveserializer.h"
#include "../move.h"

namespace Delegators {

class MoveSerializer : public IMoveSerializer {
protected:
  IMoveSerializer* m_dgate_serializer;
public:
  MoveSerializer(IMoveSerializer* serializer) 
  : m_dgate_serializer(serializer) {
    m_dgate_serializer->setDelegator(this);
  }
  virtual ~MoveSerializer() { delete m_dgate_serializer; }
  virtual QString serialize(const Move& move, const IState* ref) const {
    return m_dgate_serializer->serialize(move, ref);
  }
  virtual Move deserialize(const QString& str, const IState* ref) const {
    return m_dgate_serializer->deserialize(str, ref);
  }
  virtual QString symbol(const IType* type) const { return m_dgate_serializer->symbol(type); }
  virtual QString suffix(const Move& move, const IState* ref) const {
    return m_dgate_serializer->suffix(move, ref);
  }
  virtual QString type() const { return m_dgate_serializer->type(); }
  virtual void setDelegator(IMoveSerializer* delegator) {
    m_dgate_serializer->setDelegator(delegator);
  }
  virtual QString san(const Move& move, const IState* ref) const {
    return m_dgate_serializer->san(move, ref);
  }
};

} // namespace Delegators

#endif // DELEGATORS__MOVESERIALIZER_H


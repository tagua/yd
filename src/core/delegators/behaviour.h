/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef DELEGATORS__BEHAVIOUR_H
#define DELEGATORS__BEHAVIOUR_H

#include "../behaviour.h"

#include <KDebug>

namespace Delegators {

class Behaviour : public IBehaviour {
protected:
  IBehaviour* m_dgate_behaviour;
public:
  Behaviour(IBehaviour* delegate): m_dgate_behaviour(delegate) {
    Q_ASSERT(delegate);
    m_dgate_behaviour->setDelegator(this);
    //kDebug() << this << "m_dgate_behaviour=" << m_dgate_behaviour;
  }
  virtual ~Behaviour() { delete m_dgate_behaviour; }
  
  virtual void captureOn(IState* state, const Point& square) const {
    m_dgate_behaviour->captureOn(state, square);
  }
  virtual void move(IState* state, const Move& m) const {
    m_dgate_behaviour->move(state, m);
  }
  virtual void advanceTurn(IState* state) const {
    m_dgate_behaviour->advanceTurn(state);
  }
  virtual Point captureSquare(const IState* state, const Move& m) const {
    return m_dgate_behaviour->captureSquare(state, m);
  }
  virtual const IColor* opponent(const IColor* player) const {
    return m_dgate_behaviour->opponent(player);
  }
  virtual Point direction(const IColor* player) const { 
    return m_dgate_behaviour->direction(player);
  }
  virtual void setDelegator(IBehaviour* behaviour) {
    m_dgate_behaviour->setDelegator(behaviour);
    //kDebug() << this << "m_dgate_behaviour=" << m_dgate_behaviour;
  }
};

}

#endif // DELEGATORS__BEHAVIOUR_H

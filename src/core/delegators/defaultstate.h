#ifndef DELEGATORS__DEFAULT_STATE_H
#define DELEGATORS__DEFAULT_STATE_H

#include "../defaultstate.h"
#include "state.h"

#include <KDebug>
#include <typeinfo>

namespace Delegators {

class DefaultState : public State, public ::DefaultState {
protected:
  ::DefaultState* m_dgate_defaultstate;
public:
  DefaultState(IState* state)
    : State(state) {
    ::DefaultState* dstate = dynamic_cast< ::DefaultState*>(state);
    if (dstate == NULL) {
      kError() << "Delegators::DefaultState needs a ::DefaultState, cannot use a"
	       << typeid(state).name();
      abort();
    }

    m_dgate_defaultstate = dstate;
    m_dgate_defaultstate->setDelegator(this);
  }
  virtual ~DefaultState() { delete m_dgate_defaultstate; }

  virtual std::vector<const Point*> theoreticalMoves(const Piece& piece,
						     const Point& src) const
  { return m_dgate_defaultstate->theoreticalMoves(piece, src); }
};

} // namespace Delegators

#endif	// DELEGATORS__DEFAULT_STATE_H

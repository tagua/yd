
#ifndef CORE__DEFAULT_STATE_H
#define CORE__DEFAULT_STATE_H

#include "piece.h"
#include "point.h"
#include "state.h"

#include <vector>

class TAGUA_EXPORT DefaultState : virtual public IState {
public:
  virtual std::vector<const Point*> theoreticalMoves(const Piece& piece,
						     const Point& src) const;
};

#endif	// CORE__DEFAULT_STATE_H

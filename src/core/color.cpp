/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "color.h"

IColor::~IColor() { }

White::White() { }

QString White::name() const { return "white"; }

White* White::self() {
  static White s_instance;
  return &s_instance;
}

int White::index() const { return 0; }

Black::Black() { }

QString Black::name() const { return "black"; }

Black* Black::self() {
  static Black s_instance;
  return &s_instance;
}

int Black::index() const { return 1; }

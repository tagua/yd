#ifndef CORE__PROMOTION_MANAGER_H
#define CORE__PROMOTION_MANAGER_H

#include <QtCore/QHash>

#include "type.h"
#include "component.h"

class TAGUA_EXPORT PromotionManager: public Component {
private:
  QHash<const IType*,const IType*> m_promotions;
  QHash<const IType*,const IType*> m_depromotions;

public:
  void setPromotion(IType* from, IType* to);
  const IType * getPromotion(const IType*) const;
  const IType * getDepromotion(const IType*) const;
};

#endif // CORE__PROMOTION_MANAGER_H

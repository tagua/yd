#ifndef CORE__DEFAULT_TYPE_H
#define CORE__DEFAULT_TYPE_H

#include "type.h"

#include <vector>

class IState;
class Move;
class Piece;

typedef struct {
  unsigned x, y;
  int distance;
} MoveDefinition;

class TAGUA_EXPORT DefaultType: public IType {
public:
  virtual const std::vector<MoveDefinition> * const movesDefinitions() const = 0;
  virtual bool canMove(const Piece& piece, const Piece& target,
                       Move& move, const IState* state) const;
};

#endif	// CORE__DEFAULT_TYPE_H

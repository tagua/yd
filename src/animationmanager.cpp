/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "animationmanager.h"
#include <cmath>
#include "animation.h"
#include "mainanimation.h"
#include "mastersettings.h"
#include "core/namedsprite.h"

AnimationManager::AnimationManager() {
  m_main = new MainAnimation(1.0);

  reload();
}

AnimationManager::~AnimationManager() { delete m_main; }

void AnimationManager::reload() {
  Settings s = settings().group("animations");
  
  enabled = s.flag("enabled", true);
  maxSequence = 
      s.group("sequence").flag("enabled", true) 
    ? s.group("sequence")["max"].value<int>()
    : 0;
  movement = s["movement"].flag("enabled", true);
  explode = s["explode"].flag("enabled", true);
  fading = s["fading"].flag("enabled", true);
  transform = s["transform"].flag("enabled", true);
  
  Settings s_anim = settings().group("animations");
  int speed = (s_anim["speed"] | 16);
  int smoothness = (s_anim["smoothness"] | 16);
  m_main->setSpeed( 0.4*pow(10.0, speed/32.0) );
  m_main->setDelay( int(70.0*pow(10.0, -smoothness/32.0)) );
}

void AnimationManager::enqueue(const AnimationPtr& a) {
  m_main->addAnimation(a);
}

void AnimationManager::stop() { m_main->stop(); }

AnimationPtr AnimationManager::appear(const NamedSprite& sprite, 
                                     const QString& flags) const {
  if (fading && flags != "instant")
    return AnimationPtr(new FadeAnimation(sprite.sprite(), 0, 255));
    
  return AnimationPtr(new DropAnimation(sprite.sprite()));
}

AnimationPtr AnimationManager::disappear(const NamedSprite& sprite, 
                                        const QString& flags) const {
  if (explode && flags == "destroy")
    return AnimationPtr(new ExplodeAnimation(sprite.sprite(), Random::instance()));
    
  if (fading && flags != "instant")
    return AnimationPtr(new FadeAnimation(sprite.sprite(), 255, 0));
    
  return AnimationPtr(new CaptureAnimation(sprite.sprite()));
}

AnimationPtr AnimationManager::move(const NamedSprite& sprite, 
                                   const QPoint& destination,
                                   bool far,
                                   const QString& flags) const {
  QStringList flagList = flags.split(" ");
  bool rotate = transform && flagList.contains("rotating");
  
  if (movement && !flagList.contains("instant")) {
    if (far && flagList.contains("lshaped")) {
      return AnimationPtr(new KnightMovementAnimation(sprite.sprite(), destination, rotate));
    }
    else
      return AnimationPtr(new MovementAnimation(sprite.sprite(), destination, rotate));
  }
  
  return AnimationPtr(new InstantAnimation(sprite.sprite(), destination));
}

AnimationPtr AnimationManager::morph(const NamedSprite& sprite1,
                                    const NamedSprite& sprite2,
                                    const QString& flags) const {
  if (fading && flags != "instant")
    return AnimationPtr(new CrossFadingAnimation(sprite1.sprite(), sprite2.sprite()));

  return AnimationPtr(new PromotionAnimation(sprite1.sprite(), sprite2.sprite()));
}


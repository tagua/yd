/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef GAMEENTITY_H
#define GAMEENTITY_H

#include <core/state_fwd.h>
#include "agent.h"
#include "agentgroup.h"
#include "userentity.h"

class ChessBoard;
class Components;

class GameEntity : public UserEntity {
  Components* m_components;
  ChessBoard* m_chessboard;
  AgentGroupDispatcher m_dispatcher;
  Move m_premoveQueue;

  StatePtr doMove(const Move& move) const;
public:
  GameEntity(Components* components, const boost::shared_ptr<Game>& game,
                 ChessBoard* chessboard, AgentGroup* group);

  /**
    * Return a PGN for the game.
    */
  virtual QString save() const;

  /**
    * Load the content of a PGN inside the game.
    */
  virtual void loadPGN(const PGN& pgn);

  virtual void executeMove(const Move& move);
  virtual void addPremove(const Move& m);
  virtual void cancelPremove();

  virtual void notifyClockUpdate(int, int) { }
  virtual void notifyMove(const Index&);
  virtual void notifyBack();
  virtual void notifyForward();
  virtual void notifyGotoFirst();
  virtual void notifyGotoLast();

  virtual bool testMove(Move&) const;
  virtual bool testPremove(const Move&) const;
  virtual Piece moveHint(const Move& move) const;
  
  virtual InteractionType validTurn(const Point&) const;
  virtual InteractionType validTurn(const IColor*) const;
  virtual bool movable(const Point&) const;
  virtual bool oneClickMoves() const;

  virtual bool gotoFirst();
  virtual bool gotoLast();
  virtual bool goTo(const Index& index);
  virtual bool forward();
  virtual bool back();
  virtual bool undo();
  virtual bool redo();
  virtual bool truncate();
  virtual bool promoteVariation();

  virtual bool canDetach() const;
};

#endif // GAMEENTITY_H

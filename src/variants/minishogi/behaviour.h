/*
  Copyright (c) 2008 Yann Dirson <ydirson@altern.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef MINISHOGI__BEHAVIOUR_H
#define MINISHOGI__BEHAVIOUR_H

#include <core/delegators/behaviour.h>
#include <core/promotionmanager.h>

class Move;

namespace MiniShogi {

class TAGUA_EXPORT Behaviour : public Delegators::Behaviour {
Q_OBJECT
  Behaviour* m_delegator;
public:
  Behaviour(IBehaviour* behaviour);

public Q_SLOTS: // delegated
  void setPromotionManager(PromotionManager* pm) {
    Q_ASSERT(QMetaObject::invokeMethod(m_dgate_behaviour, "setPromotionManager",
				       Q_ARG(PromotionManager*, pm)));
  }

  const PromotionManager* promotionManager() const {
    const PromotionManager* result;
    Q_ASSERT(QMetaObject::invokeMethod(m_dgate_behaviour, "promotionManager",
				       Q_RETURN_ARG(const PromotionManager*, result)));
    return result;
  }

  bool mayPromote(const IState* state, const Move& move) const {
    bool result;
    Q_ASSERT(QMetaObject::invokeMethod(m_dgate_behaviour, "mayPromote",
				       Q_RETURN_ARG(bool, result),
				       Q_ARG(const IState*, state),
				       Q_ARG(const Move, move)));
    return result;
  }

public Q_SLOTS: // overridden
  unsigned promotionZoneWidth() const { return 1; }
};

} // namespace MiniShogi

#endif // MINISHOGI__BEHAVIOUR_H

set(taguaminishogi_SRCS
  minishogi.cpp
  behaviour.cpp
  state.cpp
)

include_directories(${CMAKE_SOURCE_DIR}/src/)

if(MONOLITH)
kde4_add_library(taguaminishogi SHARED ${taguaminishogi_SRCS})
else(MONOLITH)
kde4_add_plugin(taguaminishogi ${taguaminishogi_SRCS})
endif(MONOLITH)

target_link_libraries(taguaminishogi
  taguacore
  ${KDE4_KDECORE_LIBS}
)

install(TARGETS taguaminishogi DESTINATION ${PLUGIN_INSTALL_DIR})
install(FILES tagua-minishogi.desktop DESTINATION ${SERVICES_INSTALL_DIR})

#ifndef MINISHOGI__STATE_H
#define MINISHOGI__STATE_H

#include <core/delegators/defaultstate.h>
#include <core/component.h>
#include <core/piece.h>

/**
  * @brief Namespace holding Minishogi components.
  */
namespace MiniShogi {

class State : public Delegators::DefaultState {
Q_OBJECT
public:
  State(IState* state);
  virtual IState* clone() const;
  virtual void setup();
};

} // namespace MiniShogi

#endif // MINISHOGI__STATE_H


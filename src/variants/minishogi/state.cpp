#include "global.h"
#include "state.h"

#include <core/board.h>
#include <core/color.h>

namespace MiniShogi {

State::State(IState* state)
: Delegators::DefaultState(state) { }

IState* State::clone() const {
  return new State(m_dgate_defaultstate->clone());
}

#define COL(i,c) players[c]==White::self() ? i : board()->size().x - i - 1
void State::setup() {
  for (int c = 0; c < 2; c++) {
    const IColor* color = players[c];
    int r0 = rank(0, color);
    int r1 = rank(1, color);

    board()->set(Point(COL(0,c), r1), Piece(color, pawn));
    board()->set(Point(COL(0,c), r0), Piece(color, king));
    board()->set(Point(COL(1,c), r0), Piece(color, gold));
    board()->set(Point(COL(2,c), r0), Piece(color, silver));
    board()->set(Point(COL(3,c), r0), Piece(color, bishop));
    board()->set(Point(COL(4,c), r0), Piece(color, rook));
  }

  setTurn(White::self());
}
#undef COL

} // namespace MiniShogi

#include "behaviour.h"

namespace MiniShogi {

Behaviour::Behaviour(IBehaviour* behaviour)
  : Delegators::Behaviour(behaviour) { }

} // namespace MiniShogi

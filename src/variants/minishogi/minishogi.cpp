#include "behaviour.h"
#include "state.h"

#include <core/color.h>
#include <core/point.h>
#include <core/repository.h>
#include <core/variantloader.h>
#include <core/state.h>
#include <core/type.h>

#include <KDebug>

using namespace MiniShogi;

#define LOAD_TYPE(NAME) \
  NAME = requestInterface<IType>(shogi->getComponent("type/" #NAME))

namespace MiniShogi {
  const IType* pawn;
  const IType* king;
  const IType* gold;
  const IType* silver;
  const IType* bishop;
  const IType* rook;
  const IColor* players[2];
}

extern "C" KDE_EXPORT Repository*
taguaminishogi_initrepo(IVariantLoader* loader) {
  Repository* repo = new Repository;
  Repository* shogi = loader->getRepository("shogi");
  if (!shogi)
    // bail out if there is no shogi variant
    return 0;

  repo->setProxy(shogi);

  // get shogi state factory
  Component* shogi_state_comp = shogi->getComponent("state");
  IState* shogi_state = requestInterface<IState>(shogi_state_comp);

  // base behaviour on shogi
  Component* shogi_behaviour_comp = shogi->getComponent("behaviour");
  Q_ASSERT(shogi_behaviour_comp);
  IBehaviour* behaviour_clone = NULL;
  Q_ASSERT(QMetaObject::invokeMethod(shogi_behaviour_comp, "clone",
				     Q_RETURN_ARG(IBehaviour*, behaviour_clone),
				     Q_ARG(PromotionManager*, NULL)));
  Q_ASSERT(behaviour_clone);
  Behaviour* behaviour = new Behaviour(behaviour_clone);

  // create minishogi state factory
  IState* state_clone = 0;
  Q_ASSERT(QMetaObject::invokeMethod(shogi_state_comp, "clone",
				     Q_RETURN_ARG(IState*, state_clone),
				     Q_ARG(const IBehaviour*, behaviour),
				     Q_ARG(Point, Point(5, 5))));
  Q_ASSERT(state_clone);
  State* state = new State(state_clone);

  // set state factory
  repo->addComponent("state", state);

  LOAD_TYPE(pawn);
  LOAD_TYPE(king);
  LOAD_TYPE(gold);
  LOAD_TYPE(silver);
  LOAD_TYPE(bishop);
  LOAD_TYPE(rook);
  players[0] = Black::self();
  players[1] = White::self();

  return repo;
}

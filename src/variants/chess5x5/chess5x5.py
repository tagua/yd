from tagua.core import Repository, Point

def taguachess5x5_initrepo(loader):
  repo = Repository()
  chess = loader.get_repository('chess')
  if not chess: return None
  
  repo.proxy = chess
  repo.state_factory = chess.state_factory.createFactory(Point(5, 5))
  return repo
  
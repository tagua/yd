require 'tagua/core'

include Tagua::Core

def taguachess5x5_initrepo(loader)
  repo = Repository.new
  chess = loader.get_repository('chess') or return
  
  repo.proxy = chess
  repo.state_factory = chess.state_factory.createFactory(Point.new(5, 5))
  repo
end

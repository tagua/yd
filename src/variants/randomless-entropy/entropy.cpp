/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include <core/defaultpolicy.h>
#include <core/repository.h>
#include <core/variantloader.h>

//#include "behaviour.h"
#include "colors.h"
//#include "moveserializer.h"
#include "state.h"
#include "stone.h"
//#include "validator.h"

#include <KDebug>

using namespace RandomlessEntropy;

namespace RandomlessEntropy {
  const unsigned gamesize = 3;
}

extern "C" KDE_EXPORT Repository* taguarandomlessentropy_initrepo(IVariantLoader* loader) {
  Repository* repo = new Repository;
  Repository* chess = loader->getRepository("chess");
  if (!chess) {
    kError() << "Could not load chess variant";
    return 0;
  }

  repo->addComponent("player/0", dynamic_cast<Component*>(COLORS[0]));
  repo->addComponent("player/1", dynamic_cast<Component*>(COLORS[1]));
  
  // FIXME
  repo->addComponent("type/stone0", Stone::color(0));
  repo->addComponent("type/stone1", Stone::color(1));
  repo->addComponent("type/stone2", Stone::color(2));

  IState* chess_state = requestInterface<IState>(chess->getComponent("state"));
  const IBehaviour* behaviour = chess_state->behaviour();
  repo->addComponent("state", new State(behaviour, Point(gamesize, gamesize)));

  repo->addComponent("validator", chess->getComponent("validator")); // FIXME
  repo->addComponent("animator_factory", chess->getComponent("animator_factory"));
  repo->addComponent("namer", chess->getComponent("namer"));
  repo->addComponent("policy", new DefaultPolicy); // FIXME ?
  
#if 1						  // FIXME
  // set move serializers
  Repository::ComponentMap serializers = chess->listComponents("move_serializer");
  for (Repository::ComponentMap::const_iterator it = serializers.begin(),
       end = serializers.end(); it != end; ++it) {
    repo->addComponent("move_serializer/" + it.key(), it.value());
  }
#else
  repo->addComponent("move_serializer/simple", 
		     new MoveSerializer("simple", validator));
#endif

  return repo;
}

/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef RANDOMLESSENTROPY__STONE_H
#define RANDOMLESSENTROPY__STONE_H

#include <core/component.h>
#include <core/defaulttype.h>

namespace RandomlessEntropy {

class TAGUA_EXPORT Stone : public Component, public DefaultType {
Q_OBJECT
  Stone();
public:
  virtual QString name() const;
  virtual const std::vector<MoveDefinition> * const movesDefinitions() const;
  virtual int index() const;
  static Stone* color(unsigned color);
private:
  std::vector<MoveDefinition> m_moveDefinitions;
  unsigned m_color;
};

}

#endif // RANDOMLESSENTROPY__STONE_H

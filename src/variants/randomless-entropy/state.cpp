/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "state.h"

#include <core/behaviour.h>
#include <core/move.h>
#include <core/defaultpoolcollection.h>
#include <core/defaultpool.h>

#include "colors.h"
#include "stone.h"

namespace RandomlessEntropy {

State::State(const IBehaviour* behaviour, 
             const Point& size)
: m_board(size)
, m_behaviour(behaviour)
, m_delegator(this) {
  DefaultPoolCollection* c = new DefaultPoolCollection;
  // only Chaos/Black has a pool
  c->addPool(Black::self(), new DefaultPool(Black::self()));
  m_pools = c;
}

State::State(const State& other)
: IState()
, DefaultState()
, m_board(other.m_board)
, m_turn(other.m_turn)
, m_behaviour(other.m_behaviour)
, m_delegator(this)
, m_pools(other.m_pools) { }

State::~State() { delete m_pools; }

IState* State::clone() const {
  State* s = new State(*this);
  s->m_pools = m_pools->clone();
  return s;
}

void State::setup() {
  // Chaos starts, board is empty
  m_turn = Black::self();
  // FIXME: all pieces in pool
  IPool* pool = pools()->pool(Black::self());
  pool->insert(-1, Piece(Black::self(), Stone::color(0)));
  pool->insert(-1, Piece(Black::self(), Stone::color(1)));
  pool->insert(-1, Piece(Black::self(), Stone::color(2)));
}

const Board* State::board() const {
  return &m_board;
}

Board* State::board() {
  return &m_board;
}
  
const IColor* State::turn() const {
  return m_turn;
}
  
void State::setTurn(const IColor* turn) {
  m_turn = turn;
}
  
bool State::equals(IState* other) const {
  return m_board.equals(other->board()) &&
    m_turn == other->turn();
}

void State::assign(const IState* other) {
  m_board = *other->board();
  m_turn = other->turn();

  const IPoolCollection* pools = other->pools();
  if (pools) {
    delete m_pools;
    m_pools = pools->clone();
  }
}

void State::move(const Move& m) {
  if (m.drop() != Piece()) {
    board()->set(m.dst(), m.drop());
    pools()->pool(m.drop().color())->take(m.drop());

  } else {
    const Piece piece = m_board.get(m.src());
    if (piece == Piece()) return;

    behaviour()->move(m_delegator, m);
  }

  behaviour()->advanceTurn(m_delegator);
}

int State::rank(int n, const IColor* turn) const {
  if (turn == White::self())
    return m_board.size().y - n - 1;
  else
    return n;
}

IPoolCollection* State::pools() { return m_pools; }
const IPoolCollection* State::pools() const { return m_pools; }

const IBehaviour* State::behaviour() const { return m_behaviour; }

void State::setDelegator(IState* delegator) { m_delegator = delegator; }

Component* State::clone(const IBehaviour* behaviour, 
                        const Point& size) const {
  return new State(behaviour, size);
}

} // namespace RandomlessEntropy

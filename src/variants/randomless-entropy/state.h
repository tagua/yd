/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef RANDOMLESSENTROPY__STATE_H
#define RANDOMLESSENTROPY__STATE_H

#include <core/board.h>
#include <core/defaultstate.h>
#include <core/piece.h>
#include <core/taguaobject.h>

/**
  * @brief Namespace holding Randomless-Entropy components.
  *
  * @details Randomless-Entropy is a variant of Entropy where Chaos
  * does not randomly draws the next stone to place, but can choose
  * from all stones in the pool.
  */
namespace RandomlessEntropy {

class State : public DefaultState {
Q_OBJECT
  Board m_board;
  const IColor* m_turn;
  const IBehaviour* m_behaviour;
  IState* m_delegator;
  IPoolCollection* m_pools;
protected:
  State(const State&);
public:
  State(const IBehaviour* behaviour, 
        const Point& size);
  virtual ~State();
public:
  virtual IState* clone() const;
  
  virtual void setup();
  
  virtual const Board* board() const;
  virtual Board* board();
  
  virtual const IColor* turn() const;
  
  virtual void setTurn(const IColor* turn);
  
  virtual bool equals(IState* other) const;
  
  virtual void assign(const IState* other);

  virtual void move(const Move& move);
  
  virtual TaguaObject* flags() { return NULL; };
  virtual const TaguaObject* flags() const { return NULL; };
  
  virtual int rank(int n, const IColor* turn) const;

  virtual const IPoolCollection* pools() const;
  virtual IPoolCollection* pools();
  
  virtual const IBehaviour* behaviour() const;
  
  virtual void setDelegator(IState* delegator);
public Q_SLOTS:
  virtual Component* clone(const IBehaviour* behaviour,
                           const Point& size) const;
};

} // namespace RandomlessEntropy

#endif // RANDOMLESSENTROPY__STATE_H


/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "stone.h"

namespace RandomlessEntropy {

Stone::Stone() {
  const MoveDefinition movesDefinition[] = {
    {-1,  0, 0 },
    { 1,  0, 0 },
    { 0,  1, 0 },
    { 0, -1, 0 },
  };

  for (unsigned i = 0; i < sizeof(movesDefinition)/sizeof(movesDefinition[0]); i++)
    m_moveDefinitions.push_back(movesDefinition[i]);
}

QString Stone::name() const { return "stone" + m_color; }

const std::vector<MoveDefinition> * const Stone::movesDefinitions() const {
  return &m_moveDefinitions;
}

int Stone::index() const { return m_color; }

// FIXME: parametrize number of colors
// FIXME: find a way to set m_color in Stone constructor instead
Stone* Stone::color(unsigned color) {
  static Stone s_instance[3];
  static bool colors_initialized = false;
  if (!colors_initialized) {
    for (int i=0; i<3;i++)
      s_instance[i].m_color = i;
  }

  return &s_instance[color];
}

}

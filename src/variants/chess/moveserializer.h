/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CHESS__MOVESERIALIZER_H
#define CHESS__MOVESERIALIZER_H

#include <core/component.h>
#include <core/move.h>
#include <core/moveserializer.h>

class IState;
class IType;
class IValidator;
class SAN;

namespace Chess {

class TAGUA_EXPORT MoveSerializer : public Component, public IMoveSerializer {
protected:
  QString m_rep;
  IValidator* m_validator;
  IMoveSerializer* m_delegator;

protected Q_SLOTS:
  virtual QString san(const Move& move, const IState* ref) const;
  
  virtual void minimal_notation(SAN& san, const IState* ref) const;
  
  virtual Move get_san(const SAN& san, const IState* ref) const;
  
  virtual Move parse_ics_verbose(const QString& str, const IState* ref) const;
public:
  /** 
    * Create a serializer to a given string representation for moves.
    * \param rep A move representation type.
    */
  MoveSerializer(const QString& rep, IValidator* validator);

  virtual QString serialize(const Move& move, const IState* ref) const;

  virtual Move deserialize(const QString& str, const IState* ref) const;

  virtual QString type() const;
  
  virtual void setDelegator(IMoveSerializer* delegator);
  
  virtual QString symbol(const IType* type) const;
  
  virtual QString suffix(const Move& move, const IState* ref) const;
};

} // namespace Chess

#endif // CHESS__MOVESERIALIZER_H


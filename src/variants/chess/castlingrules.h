/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CHESS__CASTLINGRULES_H
#define CHESS__CASTLINGRULES_H

#include <core/component.h>
#include <core/point.h>

class IColor;
class IState;
class Move;
class Piece;

namespace Chess {

class ICastlingRules {
public:
  virtual ~ICastlingRules();
  
  virtual bool canCastle(const Piece& piece, Move& move, const IState* state) const = 0;
  virtual bool handleCastling(const Piece& piece, const Move& move, IState* state) const = 0;
  virtual void setDelegator(Component* rules) = 0;
  virtual Point kingStartingPosition(const IState* state, const IColor* player) const = 0;
};

class CastlingRules : public Component, public ICastlingRules {
Q_OBJECT
  ICastlingRules* m_delegator;
public:
  CastlingRules();

  virtual bool canCastle(const Piece& piece, Move& move, const IState* state) const;
  virtual bool handleCastling(const Piece& piece, const Move& move, IState* state) const;
  virtual void setDelegator(Component* rules);
  virtual Point kingStartingPosition(const IState* state, const IColor* player) const;
};

class CastlingRulesAdaptor : public ICastlingRules {
  Component* m_component;
  bool m_own;
public:
  CastlingRulesAdaptor(Component* component, bool own = true);
  virtual ~CastlingRulesAdaptor();
  
  virtual bool canCastle(const Piece& piece, Move& move, const IState* state) const;
  virtual bool handleCastling(const Piece& piece, const Move& move, IState* state) const;
  virtual void setDelegator(Component* rules);
  virtual Point kingStartingPosition(const IState* state, const IColor* player) const;
};


} // namespace Chess

#endif // CHESS__CASTLINGRULES_H


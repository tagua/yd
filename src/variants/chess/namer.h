/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CHESS__NAMER_H
#define CHESS__NAMER_H

#include <core/component.h>
#include <core/namer.h>

namespace Chess {

class Namer : public Component, public INamer {
Q_OBJECT
public Q_SLOTS:
  virtual QString name(const Piece& piece) const;
};

} // namespace Chess

#endif // CHESS__NAMER_H


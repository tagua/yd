/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef CHESS__SAN_H
#define CHESS__SAN_H

#include <QDebug>
#include <QRegExp>

#include "export.h"
#include <core/point.h>

class IType;

class TAGUA_EXPORT SAN {
  friend QDebug operator<<(QDebug os, const SAN& move);

  static QRegExp pattern;
  static QRegExp kingCastlingPattern;
  static QRegExp queenCastlingPattern;
  static QRegExp nonePattern;
  
public:
  enum CastlingType {
      NoCastling,
      KingSide,
      QueenSide
  };
  
  SAN();

  static const IType* getType(const QString& letter);

  void load(const QString&, int& offset, int ysize);
  void load(const QString&, int ysize);

  Point src, dst;
  const IType* type;
  const IType* promotion;
  CastlingType castling;
  bool drop;

  inline bool invalid() const { return (dst == Point::invalid()) && (castling == NoCastling); }
  inline bool valid() const { return !invalid(); }
};

#endif // CHESS__SAN_H

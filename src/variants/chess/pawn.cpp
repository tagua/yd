/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "pawn.h"
#include "queen.h"
#include <core/behaviour.h>
#include <core/move.h>
#include <core/pathinfo.h>
#include <core/state.h>
#include <core/board.h>
#include <core/taguacast.h>

namespace Chess {

Pawn::Pawn() { }

QString Pawn::name() const { return "pawn"; }

bool Pawn::canMove(const Piece& piece, const Piece& target,
                   Move& move, const IState* state) const {
  const IBehaviour* behaviour = state->behaviour();
  if (!behaviour) return false;
  bool enPassant = state->flags()->get("en_passant_square").value<Point>() == move.dst();

  // moving
  if (target == Piece() && !enPassant) {
    if (move.delta() == behaviour->direction(piece.color())) {
      if (move.dst().y == state->rank(state->board()->size().y - 1, piece.color())) {
        setPromotion(move, Queen::self());
      }
      return true;
    }
    
    if (move.src().y == state->rank(1, piece.color()) &&
        move.delta() == behaviour->direction(piece.color()) * 2 &&
        state->board()->get(move.src() + behaviour->direction(piece.color())) == Piece()) {
      move.setType("en_passant_trigger");
      return true;
    }
    else
      return false;
  }

  // capturing
  else if (enPassant || target.color() != piece.color()) {
    if (move.delta().y == behaviour->direction(piece.color()).y && 
        abs(move.delta().x) == 1) {
      if (enPassant)
        move.setType("en_passant_capture");
      else if (move.dst().y == state->rank(state->board()->size().y - 1, piece.color())) {
        setPromotion(move, Queen::self());
      }

      return true;
    }
  }

  return false;
}

void Pawn::setPromotion(Move& move, const IType* type) const {
  move.setType("promotion");
  // FIXME use variant data to set promotion type, like e.g.:
  // move.setPromotion(m_data.get("promotion-type").value<QObject*>())
  if (!move.promotion()) move.setPromotion(type);
}

int Pawn::index() const { return 10; }

Pawn* Pawn::self() {
  static Pawn s_instance;
  return &s_instance;
}

}

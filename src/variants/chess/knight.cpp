/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "knight.h"
#include <core/move.h>
#include <core/pathinfo.h>
#include <core/state.h>
#include <core/board.h>

namespace Chess {

Knight::Knight() { }

QString Knight::name() const { return "knight"; }

bool Knight::canMove(const Piece&, const Piece&,
                   Move& move, const IState*) const {
  return (abs(move.delta().x) == 1 && abs(move.delta().y) == 2) ||
         (abs(move.delta().y) == 1 && abs(move.delta().x) == 2);
}

int Knight::index() const { return 30; }

Knight* Knight::self() {
  static Knight s_instance;
  return &s_instance;
}

}

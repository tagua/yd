/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "king.h"
#include <core/move.h>

#include "castlingrules.h"

namespace Chess {

King::King() { }

QString King::name() const { return "king"; }

bool King::canMove(const Piece& piece, const Piece&,
                   Move& move, const IState* state) const {
  if (abs(move.delta().x) <= 1 && abs(move.delta().y) <= 1) return true;
  if (m_castling_rules)
    return m_castling_rules->canCastle(piece, move, state);
  return false;
}

int King::index() const { return 10000; }

King* King::self() {
  static King s_instance;
  return &s_instance;
}

void King::setCastlingRules(Component* castlingRules) {
  m_castling_rules = dynamic_cast<ICastlingRules*>(castlingRules);
  if (!m_castling_rules) m_castling_rules = new CastlingRulesAdaptor(castlingRules, false);
}

}

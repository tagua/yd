/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "knight.h"

namespace Shogi {

Knight::Knight() {
  MoveDefinition movesDefinition[] = {
    {-1,  2, 1 },
    { 1,  2, 1 },
  };

  for (unsigned i = 0; i < sizeof(movesDefinition)/sizeof(movesDefinition[0]); i++)
    m_moveDefinitions.push_back(movesDefinition[i]);
}

QString Knight::name() const { return "knight"; }

const std::vector<MoveDefinition> * const Knight::movesDefinitions() const {
  return &m_moveDefinitions;
}

int Knight::index() const { return 30; }

Knight* Knight::self() {
  static Knight s_instance;
  return &s_instance;
}

}

/*
  Copyright (c) 2008 Yann Dirson <ydirson@altern.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "bishop.h"

namespace Shogi {

Bishop::Bishop() {
  MoveDefinition movesDefinition[] = {
    {-1,  1, 0 },
    { 1,  1, 0 },
    {-1, -1, 0 },
    { 1, -1, 0 },
  };

  for (unsigned i = 0; i < sizeof(movesDefinition)/sizeof(movesDefinition[0]); i++)
    m_moveDefinitions.push_back(movesDefinition[i]);
}

QString Bishop::name() const { return "bishop"; }

const std::vector<MoveDefinition> * const Bishop::movesDefinitions() const {
  return &m_moveDefinitions;
}

int Bishop::index() const { return 35; }

Bishop* Bishop::self() {
  static Bishop s_instance;
  return &s_instance;
}

}

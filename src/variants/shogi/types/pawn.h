/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef SHOGI__TYPES__PAWN_H
#define SHOGI__TYPES__PAWN_H

#include <core/component.h>
#include "../type.h"

namespace Shogi {

class TAGUA_EXPORT Pawn : public Component, public Type {
Q_OBJECT
  Pawn();
public:
  virtual QString name() const;
  virtual const std::vector<MoveDefinition> * const movesDefinitions() const;
  virtual int index() const;
  static Pawn* self();
private:
  std::vector<MoveDefinition> m_moveDefinitions;
};

}

#endif // SHOGI__TYPES__PAWN_H

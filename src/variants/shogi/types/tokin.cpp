/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "tokin.h"

namespace Shogi {

Tokin::Tokin() {
  MoveDefinition movesDefinition[] = {
    {-1,  1, 1 },
    { 0,  1, 1 },
    { 1,  1, 1 },
    {-1,  0, 1 },
    { 1,  0, 1 },
    { 0, -1, 1 },
  };

  for (unsigned i = 0; i < sizeof(movesDefinition)/sizeof(movesDefinition[0]); i++)
    m_moveDefinitions.push_back(movesDefinition[i]);
}

QString Tokin::name() const { return "tokin"; }

const std::vector<MoveDefinition> * const Tokin::movesDefinitions() const {
  return &m_moveDefinitions;
}

int Tokin::index() const { return 10; }

Tokin* Tokin::self() {
  static Tokin s_instance;
  return &s_instance;
}

}

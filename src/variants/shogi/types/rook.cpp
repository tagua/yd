/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "rook.h"

namespace Shogi {

Rook::Rook() {
  MoveDefinition movesDefinition[] = {
    {-1,  0, 0 },
    { 1,  0, 0 },
    { 0,  1, 0 },
    { 0, -1, 0 },
  };

  for (unsigned i = 0; i < sizeof(movesDefinition)/sizeof(movesDefinition[0]); i++)
    m_moveDefinitions.push_back(movesDefinition[i]);
}

QString Rook::name() const { return "rook"; }

const std::vector<MoveDefinition> * const Rook::movesDefinitions() const {
  return &m_moveDefinitions;
}

int Rook::index() const { return 50; }

Rook* Rook::self() {
  static Rook s_instance;
  return &s_instance;
}

}

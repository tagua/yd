/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "narikei.h"

namespace Shogi {

Narikei::Narikei() {
  MoveDefinition movesDefinition[] = {
    {-1,  1, 1 },
    { 0,  1, 1 },
    { 1,  1, 1 },
    {-1,  0, 1 },
    { 1,  0, 1 },
    { 0, -1, 1 },
  };

  for (unsigned i = 0; i < sizeof(movesDefinition)/sizeof(movesDefinition[0]); i++)
    m_moveDefinitions.push_back(movesDefinition[i]);
}

QString Narikei::name() const { return "narikei"; }

const std::vector<MoveDefinition> * const Narikei::movesDefinitions() const {
  return &m_moveDefinitions;
}

int Narikei::index() const { return 30; }

Narikei* Narikei::self() {
  static Narikei s_instance;
  return &s_instance;
}

}

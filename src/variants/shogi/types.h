/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef SHOGI__TYPES_H
#define SHOGI__TYPES_H

// include all shogi types
#include "types/king.h"
#include "types/gold.h"
#include "types/silver.h"
#include "types/knight.h"
#include "types/lance.h"
#include "types/rook.h"
#include "types/bishop.h"
#include "types/pawn.h"

#include "types/narigin.h"
#include "types/narikei.h"
#include "types/narikyo.h"
#include "types/dragonking.h"
#include "types/dragonhorse.h"
#include "types/tokin.h"

#endif // SHOGI__TYPES_H

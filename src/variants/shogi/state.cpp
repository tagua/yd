/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "state.h"

#include <core/behaviour.h>
#include <core/move.h>
#include <core/defaultpoolcollection.h>
#include <core/defaultpool.h>

#include "colors.h"
#include "types.h"

namespace Shogi {

State::State(const IBehaviour* behaviour, 
             const Point& size)
: m_board(size)
, m_behaviour(behaviour)
, m_delegator(this) {
  DefaultPoolCollection* c = new DefaultPoolCollection;
  for (int i = 0; i < 2; i++)
    c->addPool(COLORS[i], new DefaultPool(COLORS[i]));
  m_pools = c;
}

State::State(const State& other)
: IState()
, DefaultState()
, m_board(other.m_board)
, m_flags(other.m_flags)
, m_turn(other.m_turn)
, m_behaviour(other.m_behaviour)
, m_delegator(this)
, m_pools(other.m_pools) { }

State::~State() { delete m_pools; }

IState* State::clone() const {
  State* s = new State(*this);
  s->m_pools = m_pools->clone();
  return s;
}

#define COL(i, c) c == Black::self() ? (i) : (m_board.size().x - i - 1)
void State::setup() {
  for (int c = 0; c < 2; c++) {
    IColor* color = COLORS[c];
    int r0 = rank(0, color);
    int r1 = rank(1, color);
    int r2 = rank(2, color);
    
    for (int i = 0; i < m_board.size().x; i++) {
      m_board.set(Point(i, r2), Piece(color, Pawn::self()));
    }
    m_board.set(Point(0, r0), Piece(color, Lance::self()));
    m_board.set(Point(1, r0), Piece(color, Knight::self()));
    m_board.set(Point(2, r0), Piece(color, Silver::self()));
    m_board.set(Point(3, r0), Piece(color, Gold::self()));
    m_board.set(Point(4, r0), Piece(color, King::self()));
    m_board.set(Point(5, r0), Piece(color, Gold::self()));
    m_board.set(Point(6, r0), Piece(color, Silver::self()));
    m_board.set(Point(7, r0), Piece(color, Knight::self()));
    m_board.set(Point(8, r0), Piece(color, Lance::self()));

    m_board.set(Point(COL(1, color), r1), Piece(color, Bishop::self()));
    m_board.set(Point(COL(7, color), r1), Piece(color, Rook::self()));
  }
  
  m_turn = White::self();
}
#undef COL

const Board* State::board() const {
  return &m_board;
}

Board* State::board() {
  return &m_board;
}
  
const IColor* State::turn() const {
  return m_turn;
}
  
void State::setTurn(const IColor* turn) {
  m_turn = turn;
}

// FIXME: should take pools into account
bool State::equals(IState* other) const {
  return m_board.equals(other->board()) &&
         m_turn == other->turn() &&
         m_flags == *other->flags();
}

void State::assign(const IState* other) {
  m_board = *other->board();
  m_turn = other->turn();
  m_flags = *other->flags();

  const IPoolCollection* pools = other->pools();
  if (pools) {
    delete m_pools;
    m_pools = pools->clone();
  }
}

void State::move(const Move& m) {
  if (m.drop() != Piece()) {
    //const Piece captured = board()->get(m.dst());
    board()->set(m.dst(), m.drop());
    pools()->pool(m.drop().color())->take(m.drop());

#if 0
    // handle capturing by drop: some variants could use it
    if (captured != Piece()) {
      if (captured.get("promoted").toBool()) {
        captured.setType(pawn);
      }
      pools()->pool(behaviour()->opponent(captured.color()))
             ->insert(-1, captured);
    }
#endif
  } else {
    const Piece piece = m_board.get(m.src());
    if (piece == Piece()) return;

    Point captureSquare = behaviour()->captureSquare(this, m);
    behaviour()->captureOn(m_delegator, captureSquare);
    behaviour()->move(m_delegator, m);

    const IType* promotion_type = m.promotion();

    if (promotion_type != 0) {
      m_board.set(m.dst(), Piece(piece.color(), promotion_type));
    }
  }

  behaviour()->advanceTurn(m_delegator);
}

const TaguaObject* State::flags() const { return &m_flags; }
TaguaObject* State::flags() { return &m_flags; }

int State::rank(int n, const IColor* turn) const {
  if (turn == White::self())
    return m_board.size().y - n - 1;
  else
    return n;
}

IPoolCollection* State::pools() { return m_pools; }
const IPoolCollection* State::pools() const { return m_pools; }

const IBehaviour* State::behaviour() const { return m_behaviour; }

void State::setDelegator(IState* delegator) {
  m_delegator = delegator;
}

IState* State::clone(const IBehaviour* behaviour,
		     const Point& size) const {
  return new State(behaviour, size);
}

} // namespace Shogi

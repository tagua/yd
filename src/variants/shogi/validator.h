/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef SHOGI__VALIDATOR_H
#define SHOGI__VALIDATOR_H

#include <core/component.h>
#include <core/validator.h>

namespace Shogi {

class Validator : public Component, public IValidator {
Q_OBJECT
  IValidator* m_delegator;
protected:
  virtual bool stuckPiece(const IState* state,
			  const Piece& piece, const Point& p) const;
public:
  Validator();

  virtual bool pseudolegal(const IState* state, Move& move) const;

  virtual bool legal(const IState* state, Move& move) const;

  virtual bool attacks(const IState* state, const IColor* player,
                       const Point& square, const Piece& target = Piece()) const;
                       
  virtual const IColor* mover(const IState* state, const Move& move) const;
  
  virtual void setDelegator(IValidator* validator);
};

}

#endif // SHOGI__VALIDATOR_H


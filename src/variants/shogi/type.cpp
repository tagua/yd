#include "type.h"

#include <core/behaviour.h>
#include <core/promotionmanager.h>
#include <core/state.h>
#include <core/move.h>

#include <KDebug>

namespace Shogi {

bool Type::canMove(const Piece& piece, const Piece& target,
		     Move& move, const IState* state) const {
  bool valid = DefaultType::canMove(piece, target, move, state);
  if (!valid)
    return false;

  IBehaviour* behaviour = const_cast<IBehaviour*>(state->behaviour());
  Q_ASSERT(behaviour);
  bool may_promote;
  Q_ASSERT(QMetaObject::invokeMethod(behaviour, "mayPromote",
				     Q_RETURN_ARG(bool, may_promote),
				     Q_ARG(const IState*, state),
				     Q_ARG(const Move, move)));	// FIXME: Move& ?
  if (may_promote) {
    move.setType("promotion");
    const PromotionManager* pm;
    Q_ASSERT(QMetaObject::invokeMethod(behaviour, "promotionManager",
				       Q_RETURN_ARG(const PromotionManager*, pm)));
    const IType* newtype = pm->getPromotion(piece.type());
    kDebug() << "promoting to" << newtype->name();
    move.setPromotion(newtype);
  }

  return true;
}

} // namespace Shogi

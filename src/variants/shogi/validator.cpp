#include "validator.h"
#include <memory>
#include <core/behaviour.h>
#include <core/board.h>
#include <core/move.h>
#include <core/poolcollection.h>
#include <core/pool.h>
#include <core/state.h>
#include <core/type.h>
#include "colors.h"
#include "types.h"

namespace Shogi {

Validator::Validator() : m_delegator(this) { }

bool Validator::pseudolegal(const IState* state, Move& move) const {
  // add drop information to move, if missing
  if (move.drop() == Piece() &&
      move.pool() && move.index() != -1) {
    move.setDrop(state->pools()->pool(move.pool())->get(move.index()));
  }

  Piece dropped = move.drop();
  if (dropped == Piece()) {
    if (!state->board()->valid(move.src())) return false;
    if (!state->board()->valid(move.dst())) return false;
  
    const Piece piece = state->board()->get(move.src());
    if (piece == Piece()) return false;
  
    const IBehaviour* behaviour = state->behaviour();
    if (!behaviour) return false;
  
    const IColor* thisTurn = piece.color();
    if (state->turn() != thisTurn) return false;
  
    const Piece target = state->board()->get(move.dst());
    if (target.color() == piece.color())
      return false;
    if (!piece.type()->canMove(piece, target, move, state))
      return false;

#if 0
    if (move.type() == "promotion") {
      const IType* promotionType = move.promotion();
      if (promotionType != Queen::self() &&
	  promotionType != Bishop::self() &&
	  promotionType != Rook::self() &&
	  promotionType != Knight::self()) return false;
    }
#endif
  } else {
    // dropping on a valid square
    if (!state->board()->valid(move.dst()))
      return false;

    // cannot drop on occupied squares
    if (state->board()->get(move.dst()) != Piece())
      return false;

    // cannot drop on a place where piece would be stuck
    if (stuckPiece(state, dropped, move.dst()))
      return false;

    // cannot drop a pawn in a column with already a pawn of same color
    if (dropped.type() == Pawn::self()) {
      for (int i = 0; i < state->board()->size().y; i++) {
        const Piece other = state->board()->get(Point(move.dst().x, i));
        if (other.type() == Pawn::self() &&
            other.color() == state->turn() &&
            !other.get("promoted").toBool())
          return false;
      }
    }
  }

  return true;
}

bool Validator::legal(const IState* state, Move& move) const {
  // do not check a move more than once
  if (!move.type().isEmpty()) return true;

  if (!m_delegator->pseudolegal(state, move))
    return false;
    
  const IBehaviour* behaviour = state->behaviour();
  if (!behaviour) return false;
    
  const IColor* turn = m_delegator->mover(state, move);
  
  IState* tmp = state->clone();
  Q_ASSERT(tmp);
  tmp->move(move);
  
  Point kingPos = tmp->board()->find(Piece(turn, King::self()));
  
  if (kingPos == Point::invalid()) {
    delete tmp;
    return false;
  }

  if (m_delegator->attacks(tmp, behaviour->opponent(turn), kingPos)) {
    delete tmp;
    return false;
  }

  delete tmp;

  // set move type as normal, if not already set    
  if (move.type().isEmpty()) move.setType("normal");
  return true;
}

// FIXME: move this logic into Piece classes ?
bool Validator::stuckPiece(const IState* state,
			   const Piece& piece, const Point& p) const {
  const IBehaviour* behaviour = state->behaviour();

  if (piece.type() == Pawn::self() || piece.type() == Lance::self()) {
    return p.y == state->rank(0, behaviour->opponent(piece.color()));
  }
  else if (piece.type() == Knight::self()) {
    int rank = state->rank(0, behaviour->opponent(piece.color()));
    return p.y == rank || p.y == rank - behaviour->direction(piece.color()).y;
  }
  else {
    return false;
  }
}

bool Validator::attacks(const IState* state, const IColor* player,
                        const Point& square, const Piece& target_) const {
  Piece target;
  if (target_ != Piece())
    target = target_;
  else
    target = state->board()->get(square);

  for (int i = 0; i < state->board()->size().x; i++) {
    for (int j = 0; j < state->board()->size().y; j++) {
      const Point p(i, j);
      const Piece piece = state->board()->get(p);
      Move move(p, square);
      if (piece != Piece() &&
          piece.color() == player &&
          piece.type()->canMove(piece, target, move, state))
        return true;
    }
  }
  return false;
}

const IColor* Validator::mover(const IState* state, const Move& move) const {
  return state->board()->get(move.src()).color();
}

void Validator::setDelegator(IValidator* delegator) {
  m_delegator = delegator;
}

}


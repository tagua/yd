
#include "moveserializer.h"

#include <core/board.h>
#include <core/state.h>

namespace Shogi {

MoveSerializer::MoveSerializer(IMoveSerializer* serializer)
: Delegators::MoveSerializer(serializer) { }

QString MoveSerializer::serialize(const Move& move, const IState* ref) const {
  if (type() == "simple") {
    int ysize = ref->board()->size().y;
    QString res = move.src().toString(ysize) + move.dst().toString(ysize);
    if (move.promotion())
      res = res + "=" + 
        symbol(
          move.promotion()
        ).toUpper();
    return res;
  }
  else if (type() == "compact") {
    return san(move, ref);
  }
  else if (type() == "decorated") {
    QString res = san(move, ref);
    res.replace('K', "{king}");
    res.replace('G', "{gold}");
    res.replace('S', "{silver}");
    res.replace('N', "{knight}");
    res.replace('L', "{lance}");
    res.replace('B', "{bishop}");
    res.replace('R', "{rook}");
    res.replace('P', "{pawn}");
    return res;
  }
  else {
    return "";
  }
}

} // namespace Shogi

#include <core/repository.h>
#include <core/variantloader.h>

#include "behaviour.h"
#include "colors.h"
#include "moveserializer.h"
#include "types.h"
#include "state.h"
#include "validator.h"

using namespace Shogi;

extern "C" KDE_EXPORT Repository*
taguashogi_initrepo(IVariantLoader* loader) {
  Repository* repo = new Repository;
  Repository* chess = loader->getRepository("chess");
  if (!chess)
    // bail out if there is no chess variant
    return 0;
#if 0
  Repository* crazyhouse = loader->getRepository("crazyhouse");
  if (!crazyhouse)
    // bail out if there is no crazyhouse variant
    return 0;
#endif

  repo->addComponent("player/0", dynamic_cast<Component*>(COLORS[0]));
  repo->addComponent("player/1", dynamic_cast<Component*>(COLORS[1]));
  
  repo->addComponent("type/king", King::self());
  repo->addComponent("type/gold", Gold::self());
  repo->addComponent("type/silver", Silver::self());
  repo->addComponent("type/knight", Knight::self());
  repo->addComponent("type/lance", Lance::self());
  repo->addComponent("type/rook", Rook::self());
  repo->addComponent("type/bishop", Bishop::self());
  repo->addComponent("type/pawn", Pawn::self());

  repo->addComponent("type/narigin", Narigin::self());
  repo->addComponent("type/narikei", Narikei::self());
  repo->addComponent("type/narikyo", Narikyo::self());
  repo->addComponent("type/dragonking", DragonKing::self());
  repo->addComponent("type/dragonhorse", DragonHorse::self());
  repo->addComponent("type/tokin", Tokin::self());

  PromotionManager* promotion_manager = new PromotionManager();
  promotion_manager->setPromotion(Silver::self(), Narigin::self());
  promotion_manager->setPromotion(Knight::self(), Narikei::self());
  promotion_manager->setPromotion(Lance::self(), Narikyo::self());
  promotion_manager->setPromotion(Rook::self(), DragonKing::self());
  promotion_manager->setPromotion(Bishop::self(), DragonHorse::self());
  promotion_manager->setPromotion(Pawn::self(), Tokin::self());
  repo->addComponent("promotion_manager", promotion_manager);

  // base behaviour on chess
  Component* chess_behaviour_comp = chess->getComponent("behaviour");
  Q_ASSERT(chess_behaviour_comp);
  IBehaviour* behaviour_clone = NULL;
  Q_ASSERT(QMetaObject::invokeMethod(chess_behaviour_comp, "clone",
				     Q_RETURN_ARG(IBehaviour*, behaviour_clone)));
  Q_ASSERT(behaviour_clone);
  Behaviour* behaviour = new Behaviour(behaviour_clone);
  behaviour->setPromotionManager(promotion_manager);
  repo->addComponent("behaviour", behaviour);
  repo->addComponent("state", new State(behaviour, Point(9, 9)));

  Validator* validator = new Validator;
  repo->addComponent("validator", validator);
  repo->addComponent("animator_factory", chess->getComponent("animator_factory"));
  repo->addComponent("namer", chess->getComponent("namer"));
  repo->addComponent("policy", chess->getComponent("policy"));
  
  // set move serializers
  Repository::ComponentMap serializers = chess->listComponents("move_serializer");
  for (Repository::ComponentMap::const_iterator it = serializers.begin(),
       end = serializers.end(); it != end; ++it) {
    IMoveSerializer* s = requestInterface<IMoveSerializer>(it.value());
    if (s) repo->addComponent("move_serializer/" + it.key(), new MoveSerializer(s));
  }
  
  return repo;
}


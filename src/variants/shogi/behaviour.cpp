/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "behaviour.h"

#include <core/board.h>
#include <core/color.h>
#include <core/move.h>
#include <core/piece.h>
#include <core/poolcollection.h>
#include <core/pool.h>
#include <core/state.h>

#include <KDebug>

namespace Shogi {

Behaviour::Behaviour(IBehaviour* behaviour)
: Delegators::Behaviour(behaviour)
, m_promotionmanager(NULL)
, m_delegator(this) { }

IBehaviour* Behaviour::clone() const {
  IBehaviour* dgate_clone;
  Q_ASSERT(QMetaObject::invokeMethod(m_dgate_behaviour, "clone",
				     Q_RETURN_ARG(IBehaviour*, dgate_clone)));
  Behaviour* b = new Behaviour(dgate_clone);
  //FIXME: b->m_promotionmanager = m_promotionmanager->clone();
  b->m_promotionmanager = m_promotionmanager;
  return b;
}

void Behaviour::captureOn(IState* state, const Point& square) const {
  Piece captured = state->board()->get(square);
  if (captured != Piece()) {
    if (captured.get("promoted").toBool()) {
      if (promotionManager())
	captured.setType(promotionManager()->getDepromotion(captured.type()));
    }
    state->pools()->pool(opponent(captured.color()))->insert(-1, captured);
  }
  m_dgate_behaviour->captureOn(state, square);
}

bool Behaviour::mayPromote(const IState* state, const Move& move) const {
  // promoted piece cannot promote again
  if (move.promotion())
    return false;

  // does that piece type can even promote ?
  if (promotionManager() == NULL ||
      promotionManager()->getPromotion(state->board()->get(move.src()).type()) == NULL)
    return false;

  // move starts or ends on the promotion rows on the other side ?
  unsigned pzwidth;
  Q_ASSERT(QMetaObject::invokeMethod(m_delegator, "promotionZoneWidth",
				     Q_RETURN_ARG(unsigned, pzwidth)));
  return ((state->turn() == Black::self()) ?
	  (move.src().y >= state->board()->size().y - pzwidth ||
	   move.dst().y >= state->board()->size().y - pzwidth) :
	  (move.src().y < pzwidth ||
	   move.dst().y < pzwidth));
}

void Behaviour::setDelegator(IBehaviour* behaviour) {
  Delegators::Behaviour::setDelegator(behaviour);
  m_delegator = behaviour;
}

IBehaviour* Behaviour::clone(PromotionManager* pm) const {
  Behaviour* b = dynamic_cast<Behaviour*>(clone()); // FIXME: cannot use static_cast ?
  Q_ASSERT(b);
  if (pm)
    b->m_promotionmanager = pm;
  return b;
}

} // namespace Shogi

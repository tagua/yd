/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef SHOGI__BEHAVIOUR_H
#define SHOGI__BEHAVIOUR_H

#include <core/delegators/behaviour.h>
#include <core/promotionmanager.h>

class Move;

namespace Shogi {

class TAGUA_EXPORT Behaviour : public Delegators::Behaviour {
Q_OBJECT
  PromotionManager* m_promotionmanager;
  IBehaviour* m_delegator;
public:
  Behaviour(IBehaviour* behaviour);
  virtual IBehaviour* clone() const;
  virtual void captureOn(IState* state, const Point& square) const;
  virtual void setDelegator(IBehaviour* behaviour);

public Q_SLOTS:
  void setPromotionManager(PromotionManager* pm) {
    m_promotionmanager = pm;
    kDebug() << this << "m_promotionmanager=" << m_promotionmanager;
  }
  const PromotionManager* promotionManager() const {
    if (!m_promotionmanager)
      kError() << "no promotion manager";
    return m_promotionmanager;
  }

  bool mayPromote(const IState* state, const Move& move) const;
  unsigned promotionZoneWidth() const { return 3; }

  virtual IBehaviour* clone(PromotionManager* pm) const;
};

} // namespace Shogi

#endif // SHOGI__BEHAVIOUR_H

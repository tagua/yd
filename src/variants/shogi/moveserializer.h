#ifndef SHOGI__MOVESERIALIZER_H
#define SHOGI__MOVESERIALIZER_H

#include <core/component.h>
#include <core/delegators/moveserializer.h>

namespace Shogi {
class TAGUA_EXPORT MoveSerializer : public Component, public Delegators::MoveSerializer {
public:
  MoveSerializer(IMoveSerializer* serializer);

  virtual QString serialize(const Move& move, const IState* ref) const;
};

} // namespace Shogi

#endif // SHOGI__MOVESERIALIZER_H


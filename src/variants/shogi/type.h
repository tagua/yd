/*
  Copyright (c) 2008 Yann Dirson <ydirson@altern.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef SHOGI__TYPE_H
#define SHOGI__TYPE_H

#include <core/defaulttype.h>

namespace Shogi {

class TAGUA_EXPORT Type : public DefaultType {
public:
  virtual const std::vector<MoveDefinition> * const movesDefinitions() const = 0;
  virtual bool canMove(const Piece& piece, const Piece& target,
                       Move& move, const IState* state) const;
};

}

#endif // SHOGI__TYPE_H

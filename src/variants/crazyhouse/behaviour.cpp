/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "behaviour.h"

#include <core/board.h>
#include <core/piece.h>
#include <core/poolcollection.h>
#include <core/pool.h>
#include <core/state.h>
#include "global.h"

namespace Crazyhouse {

Behaviour::Behaviour(IBehaviour* behaviour)
: Delegators::Behaviour(behaviour) { }

IBehaviour* Behaviour::clone() const {
  IBehaviour* dgate_clone;
  Q_ASSERT(QMetaObject::invokeMethod(m_dgate_behaviour, "clone",
				     Q_RETURN_ARG(IBehaviour*, dgate_clone)));
  return new Behaviour(dgate_clone);
}

void Behaviour::captureOn(IState* state, const Point& square) const {
  Piece captured = state->board()->get(square);
  if (captured != Piece()) {
    if (captured.get("promoted").toBool()) {
      captured.setType(pawn);
    }
    state->pools()->pool(opponent(captured.color()))->insert(-1, captured);
  }
  m_dgate_behaviour->captureOn(state, square);
}

} // namespace Crazyhouse



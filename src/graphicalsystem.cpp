/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "graphicalsystem.h"

#include <KDebug>

#include <core/animator.h>
#include <core/board.h>
#include <core/color.h>
#include <core/namer.h>
#include <core/state.h>

#include "chesstable.h"
#include "chessboard.h"
#include "clock.h"
#include "components.h"
#include "piecepool.h"
#include "pointconverter.h"
#include "sprite.h"
#include "animation.h"
#include "pref_theme.h"
#include "movelist_table.h"
#include "mastersettings.h"

using namespace boost;

//BEGIN GraphicalSystem

GraphicalSystem::GraphicalSystem(ChessTable* view,
                             const StatePtr& startingPosition,
                             Components* components)
: m_view(view)
, m_components(components) {
  m_state = StatePtr(startingPosition->clone());
  Point s = m_state->board()->size();
  for(int i=0;i<s.x;i++)
  for(int j=0;j<s.y;j++)
    m_state->board()->set(Point(i,j), Piece());

  m_board = view->board();

  m_board->createGrid(m_state->board()->size(), m_state->board()->borderCoords());
  m_board->reset();

  m_view->pool(0)->clear();
  m_view->pool(1)->clear();
  
  m_animator = m_components->createAnimator(this);
  
  settings().onChange(this, "settingsChanged", "Loader::Theme");
  settingsChanged();

  if (startingPosition)
    warp(Move(), startingPosition);
}

GraphicalSystem::~GraphicalSystem() {
  delete m_animator;
}

const PointConverter* GraphicalSystem::converter() const {
  return m_board->converter();
}

const IndexConverter* GraphicalSystem::indexConverter(int pool) const {
  return m_view->pool(pool)->converter();
}

const IState* GraphicalSystem::state() const {
  return m_state.get();
}

void GraphicalSystem::settingsChanged() {
  ThemeInfo theme = PrefTheme::getBestTheme(m_components->variant(), "pieces");
  ThemeInfo sqtheme = PrefTheme::getBestTheme(m_components->variant(), "squares");
  ThemeInfo figtheme = PrefTheme::getBestTheme(m_components->variant(), "figurines");
  ThemeInfo ctrltheme = PrefTheme::getBestTheme(m_components->variant(), "controls");

  m_board->loader()->setTheme(theme);
  m_board->tagsLoader()->setTheme(sqtheme);
  m_board->controlsLoader()->setTheme(ctrltheme);

  for(int i=0;i<2;i++)
    m_view->pool(i)->loader()->setTheme(theme);

  for(int i=0;i<2;i++)
    m_view->clock(i)->controlsLoader()->setTheme(ctrltheme);

  m_view->moveListTable()->setLoaderTheme(figtheme);
  m_view->moveListTable()->settingsChanged();

  //clear board and pool, forcing reload
  m_view->settingsChanged();
}

void GraphicalSystem::setup(const shared_ptr<UserEntity>& entity) {
  m_view->setEntity(entity);
}

NamedSprite GraphicalSystem::getSprite(const Point& p) {
  if (!m_board->m_sprites.valid(p))
    return NamedSprite();

  return m_board->m_sprites[p];
}

NamedSprite GraphicalSystem::takeSprite(const Point& p) {
  if (!m_board->m_sprites.valid(p))
    return NamedSprite();

  NamedSprite retv = m_board->m_sprites[p];
  m_board->m_sprites[p] = NamedSprite();
  return retv;
}

NamedSprite GraphicalSystem::setPiece(const Point& p, const Piece& piece, bool show) {
  return m_board->m_sprites[p] = createPiece(p, piece, show);
}

NamedSprite GraphicalSystem::createPiece(const Point& p, const Piece& piece, bool show) {
  Q_ASSERT(piece != Piece());
  if (!m_board->m_sprites.valid(p))
    return NamedSprite();

  QString name = m_components->namer()->name(piece);
  NamedSprite s(name, SpritePtr(new Sprite(m_board->loadSprite(name),
                                m_board->piecesGroup(), m_board->converter()->toReal(p))));
  if (show)
    s.sprite()->show();
  return s;
}

void GraphicalSystem::setSprite(const Point& p, const NamedSprite& sprite) {
  if (!m_board->m_sprites.valid(p))
    return;

  m_board->m_sprites[p] = sprite;
}

int GraphicalSystem::poolSize(int pool) {
  return m_view->pool(pool)->fill();
}

NamedSprite GraphicalSystem::getPoolSprite(const IColor* pool, int index) {
  return m_view->pool(pool->index())->getSprite(index);
}

void GraphicalSystem::removePoolSprite(const IColor* pool, int index) {
  m_view->pool(pool->index())->removeSprite(index);
}

NamedSprite GraphicalSystem::takePoolSprite(const IColor* pool, int index) {
  return m_view->pool(pool->index())->takeSprite(index);
}

NamedSprite GraphicalSystem::insertPoolPiece(const IColor* pool, 
                                             int index, 
                                             const Piece& piece) {
  PiecePool *pl = m_view->pool(pool->index());
  QString name = m_components->namer()->name(piece);
  QPixmap px = pl->loadSprite(name);

  NamedSprite s(name, SpritePtr(new Sprite(px, pl, QPoint())));
  pl->insertSprite(index, s);
  return s;
}

std::pair<const IColor*, int> GraphicalSystem::droppedPoolPiece() {
  return std::pair<const IColor*, int>(m_board->m_dropped_pool, m_board->m_dropped_index);
}

void GraphicalSystem::forward(const Move& move,
                              const StatePtr& pos,
  const SpritePtr& /*movingSprite*/) {
  Piece sel1 = m_state->board()->get(m_board->selection);

  if (move != Move()) {
    AnimationPtr animation = m_animator->forward(move, pos.get());
    m_view->animationManager()->enqueue(animation);
    m_board->setTags("highlighting", move.src(), move.dst());

    m_state->assign(pos.get());
  }
  else
    warp(Move(), pos);

  Piece sel2 = m_state->board()->get(m_board->selection);
  if(!(sel1 != Piece() && sel2 != Piece() && sel1 == sel2))
    m_board->cancelSelection();

  m_board->cancelPremove();
  m_view->updateTurn(pos->turn()->index());
  m_board->onPositionChanged();
}

void GraphicalSystem::back(const Move& lastMove,
                           const Move& move,
                           const StatePtr& state) {
  Piece sel1 = m_state->board()->get(m_board->selection);

  if (move != Move()) {
    AnimationPtr animation = m_animator->back(move, state.get());
    //??? animation->setChainAbortions(false);
    m_view->animationManager()->enqueue(animation);

    m_state->assign(state.get());
  }
  else
    warp(lastMove, state);

  if (lastMove != Move())
    m_board->setTags("highlighting", lastMove.src(), lastMove.dst());
  else
    m_board->clearTags("highlighting");

  Piece sel2 = m_state->board()->get(m_board->selection);
  if(!(sel1 != Piece() && sel2 != Piece() && sel1 == sel2))
    m_board->cancelSelection();
  m_board->cancelPremove();
  m_view->updateTurn(state->turn()->index());
  m_board->onPositionChanged();
}

void GraphicalSystem::warp(const Move& lastMove,
                           const StatePtr& state) {

  Piece sel1 = m_state->board()->get(m_board->selection);

  AnimationPtr animation = m_animator->warp(state.get());
  //??? animation->setChainAbortions(false);
  if (animation) {
    m_view->animationManager()->enqueue(animation);
  }

  m_state->assign(state.get());

  if (lastMove != Move())
    m_board->setTags("highlighting", lastMove.src(), lastMove.dst());
  else
    m_board->clearTags("highlighting");

  Piece sel2 = m_state->board()->get(m_board->selection);
  if(!(sel1 != Piece() && sel2 != Piece() && sel1 == sel2))
    m_board->cancelSelection();
  m_view->updateTurn(state->turn()->index());
  m_board->onPositionChanged();
}

void GraphicalSystem::adjustSprite(const Point& p) {
  SpritePtr sprite = m_board->m_sprites[p].sprite();
  Q_ASSERT(sprite);
  QPoint destination = m_board->converter()->toReal(p);
  shared_ptr<Animation> animation(new MovementAnimation(sprite, destination));
  m_view->animationManager()->enqueue(animation);
}

void GraphicalSystem::setTurn(const IColor* turn) {
  m_state->setTurn(turn);
  m_view->updateTurn(m_state->turn()->index());
}

AnimationPtr GraphicalSystem::group(const QString&) const {
  return AnimationPtr(new AnimationGroup);
}

AnimationPtr GraphicalSystem::appear(const NamedSprite& sprite, 
                                     const QString& flags) const {
  return m_view->animationManager()->appear(sprite, flags);
}

AnimationPtr GraphicalSystem::disappear(const NamedSprite& sprite, 
                                        const QString& flags) const {
  return m_view->animationManager()->disappear(sprite, flags);
}

AnimationPtr GraphicalSystem::move(const NamedSprite& sprite, 
                                   const Point& destination,
                                   const QString& flags) const {
  QPoint real = converter()->toReal(destination);
  Point origin = converter()->toLogical(sprite.sprite()->pos() + 
    Point(converter()->squareSize(), converter()->squareSize()) / 2);
  return m_view->animationManager()->move(sprite, real, origin != destination, flags);
}

AnimationPtr GraphicalSystem::move(const NamedSprite& sprite, 
                                   int pool, int destination,
                                   const QString& flags) const {
  QPoint real = indexConverter(pool)->toReal(destination);
  return m_view->animationManager()->move(sprite, real, true, flags);
}

AnimationPtr GraphicalSystem::morph(const NamedSprite& sprite1,
                                    const NamedSprite& sprite2,
                                    const QString& flags) const {
  return m_view->animationManager()->morph(sprite1, sprite2, flags);
}

Components* GraphicalSystem::components() const { return m_components; }

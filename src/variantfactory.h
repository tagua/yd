/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/


#ifndef VARIANTFACTORY_H
#define VARIANTFACTORY_H

#include <QStringList>
#include "core/variantloader.h"

class Variant;
class Repository;

class VariantFactory : public IVariantLoader {
  VariantFactory();
  
public:
  Variant* create(const QString& name);

  /**
    * \return a list of all non-hidden variants.
    */
  QStringList all() const;
  
  /**
    * \return the singleton IVariantLoader instance
    */
  static VariantFactory& self();
  
  virtual Repository* getRepository(const QString& variant);
};

#endif // VARIANTFACTORY_H

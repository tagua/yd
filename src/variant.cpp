/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2007 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "variant.h"

Variant::Variant(const QString& name, Repository* repo,
                 const QString& proxy, bool hidden)
: m_name(name)
, m_proxy(proxy)
, m_hidden(hidden)
, m_repo(repo) { }

Variant::~Variant() {
  delete m_repo;
}

QString Variant::proxy() const { return m_proxy; }

QString Variant::name() const { return m_name; }

bool Variant::hidden() const { return m_hidden; }

Repository* Variant::repository() { return m_repo; }
const Repository* Variant::repository() const { return m_repo; }


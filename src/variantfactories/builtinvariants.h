/*
  Copyright (c) 2007 Paolo Capriotti <p.capriotti@gmail.com>
  Copyright (c) 2008 Yann Dirson <ydirson@altern.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#ifndef BUILTINVARIANTS_H
#define BUILTINVARIANTS_H

#include <core/variantloader.h>
#include <QStringList>

class Repository;
class Variant;

class BuiltinVariants: public IVariantLoader {
  QMap<QString,Repository*> m_variants;
  QStringList m_allvariants;
public:
  BuiltinVariants();
  Variant* create(const QString& name);
  virtual QStringList all() const;
  virtual Repository* getRepository(const QString& variant);

  /**
    * \return the singleton IVariantLoader instance
    */
  static BuiltinVariants& self();
};

#endif // BUILTINVARIANTS_H

/*
  Copyright (c) 2006 Paolo Capriotti <p.capriotti@gmail.com>
            (c) 2006 Maurizio Monge <maurizio.monge@kdemail.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
*/

#include "kdepluginvariants.h"
#include <memory>
#include <KDebug>
#include "foreach.h"
#include "variant.h"

void load_plugins();

typedef Repository* PluginFactory(IVariantLoader*);

Repository* KDEPluginVariants::load(const KService::Ptr& plugin) {
    // load plugin
  QString symbol = plugin->library() + "_initrepo";
  KLibrary pluginLib(plugin->library());
  
  // resolve initrepo function
  void* initrepo_ = pluginLib.resolveSymbol(qPrintable(symbol));
  if (!initrepo_) {
    kWarning() << "Error loading plugin" << plugin->library();
    kWarning() << pluginLib.errorString();
    return false;
  }
  PluginFactory* initrepo = reinterpret_cast<PluginFactory*>(initrepo_);
  
  return (*initrepo)(this);
}

KDEPluginVariants::KDEPluginVariants() { }

KDEPluginVariants& KDEPluginVariants::self() {
  static KDEPluginVariants inst;
  return inst;
}

Variant* KDEPluginVariants::create(const KService::Ptr& plugin) {
  Repository* repo = load(plugin);
  if (repo)
    return new Variant(plugin->name(), repo,
      plugin->property("X-Tagua-Proxy").toString());
      
  return NULL;
}

Variant* KDEPluginVariants::create(const QString& name) {
  KService::Ptr service = get(name);
  return service ? create(service) : 0;
}

KService::Ptr KDEPluginVariants::get(const QString& name) {
  KService::List services = 
    KServiceTypeTrader::self()->query("Tagua/Variant", 
                                      "Name =~ '" + name + "'");
  if (services.empty())
    return KService::Ptr();
  else
    return services[0];
}

QStringList KDEPluginVariants::all() const {
  KService::List klist = KServiceTypeTrader::self()->query(
    "Tagua/Variant", "[X-Tagua-Hidden] == false");
  QStringList result;
  foreach (KService::Ptr service, klist) {
    result << service->name();
  }
  return result;
}

Repository* KDEPluginVariants::getRepository(const QString& variant) {
  KService::Ptr service = get(variant);
  if (!service) return 0;
  
  return load(service);
}
